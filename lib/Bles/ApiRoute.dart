
class ApiRoutes {

  static String generalGet(String urlExtention) {
    return urlExtention;
  }

  static String getMainCat() {
    return "get-main-category";
  }

  static String getSliders() {
    return "get-sliders";
  }

  static String getProductsByCat(int catID) {
    return "get-products?cat=${catID}";
  }

  static String getSubCatByCat(int catID) {
    return "get-sub-cats?cat=${catID}";
  }

  static String singleProduct(int productID) {
    return "get-product?id=${productID}";
  }

  static String make_order() {
    return "make_order";
  }

  static String check_coupon(String coupon) {
    return "check_coupon?coupon=$coupon";
  }

  static String create_user() {
    return "create_user";
  }

  static String getUserInfo(int userID) {
    return "get-user-info?id=${userID}";
  }

  static String getUserOrders() {
    return "get_user_orders";
  }

  static String search(String search) {
    return "search?search=$search";
  }

  static String login() {
    return "login";
  }

  static String getProductsDiscount() {
    return "get-products-discount";
  }

  static String updateUserInfo() {
    return "update-user-info";
  }

  static String addWishlist(int productID) {
    return "add-wishlist?product=${productID}";
  }

  static String getWishlist() {
    return "get-wishlist";
  }

  static String deleteWishList(int productID) {
    return "delete-wishlist?product=${productID}";
  }
}

class ApiRoutesUpdate {
  static String baseUrl_client = "http://162.0.233.118:3000/api/";

  getLink(String url) {
    print("url ------>>>>   " + baseUrl_client + url);
    return baseUrl_client + url;
  }
}
