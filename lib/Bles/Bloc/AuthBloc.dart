
import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:point_zero/Bles/Model/Requests/LoginRequest.dart';
import 'package:point_zero/Bles/Model/Requests/MakeOrderRequest.dart';
import 'package:point_zero/Bles/Model/Requests/SignUpRequest.dart';
import 'package:point_zero/Bles/Model/Requests/update_user_info_request.dart';
import 'package:point_zero/Bles/Model/Responses/CatResponse.dart';
import 'package:point_zero/Bles/Model/Responses/GetWishListResponse.dart';
import 'package:point_zero/Bles/Model/Responses/LoginResponse.dart';
import 'package:point_zero/Bles/Model/Responses/ProducResponse.dart';
import 'package:point_zero/Bles/Model/Responses/ProductsResponse.dart';
import 'package:point_zero/Bles/Model/Responses/SignUpResponse.dart';
import 'package:point_zero/Bles/Model/Responses/SliderResponse.dart';
import 'package:point_zero/Bles/Model/Responses/SubCatResponse.dart';
import 'package:point_zero/Bles/Model/Responses/coubon_found_response.dart';
import 'package:point_zero/Bles/Model/Responses/products_discount_respones.dart';
import 'package:point_zero/Bles/Model/Responses/sjopping_list_response.dart';
import 'package:point_zero/Bles/Model/Responses/wishlist_response.dart';
import 'package:point_zero/utils/base/BaseBloc.dart';
import 'package:point_zero/utils/base/BaseResponse.dart';
import 'package:rxdart/rxdart.dart';
import '../ApiRoute.dart';

class AuthBloc extends BaseBloc {

  BehaviorSubject<LoginResponse> _login = BehaviorSubject<LoginResponse>();
  BehaviorSubject<SignUpResponse> _signup = BehaviorSubject<SignUpResponse>();
  BehaviorSubject<CatResponse> _cats = BehaviorSubject<CatResponse>();
  BehaviorSubject<SliderResponse> _slider = BehaviorSubject<SliderResponse>();
  BehaviorSubject<ProductsResponse> _products = BehaviorSubject<ProductsResponse>();
  BehaviorSubject<SubCatResponse> _subCat = BehaviorSubject<SubCatResponse>();
  BehaviorSubject<BaseResponse> _makeOrder = BehaviorSubject<BaseResponse>();
  BehaviorSubject<UserBean> _userInfo = BehaviorSubject<UserBean>();
  BehaviorSubject<ShoppingListResponse> _userOrders = BehaviorSubject<ShoppingListResponse>();
  BehaviorSubject<ProductsResponse> _search = BehaviorSubject<ProductsResponse>();
  BehaviorSubject<ProducResponse> _product = BehaviorSubject<ProducResponse>();
  BehaviorSubject<ProductsDiscountRespones> _productsDiscount = BehaviorSubject<ProductsDiscountRespones>();
  BehaviorSubject<BaseResponse> _addWishList = BehaviorSubject<BaseResponse>();
  BehaviorSubject<BaseResponse> _deleteWishList = BehaviorSubject<BaseResponse>();
  BehaviorSubject<GetWishListResponse> _getWishList = BehaviorSubject<GetWishListResponse>();

  BehaviorSubject<LoginResponse> get s_login => _login;
  BehaviorSubject<SignUpResponse> get s_signup => _signup;
  BehaviorSubject<CatResponse> get cats => _cats;
  BehaviorSubject<SliderResponse> get slider => _slider;
  BehaviorSubject<ProductsResponse> get products => _products;
  BehaviorSubject<SubCatResponse> get subCat => _subCat;
  BehaviorSubject<ProducResponse> get product => _product;
  BehaviorSubject<BaseResponse> get makeOrder => _makeOrder;
  BehaviorSubject<BaseResponse> get userInfo => _makeOrder;
  BehaviorSubject<ShoppingListResponse> get userOrders => _userOrders;
  BehaviorSubject<BaseResponse> get search => _search;
  BehaviorSubject<ProductsDiscountRespones> get productsDiscount => _productsDiscount;
  BehaviorSubject<BaseResponse> get addWishList => _addWishList;
  BehaviorSubject<BaseResponse> get deleteWishList => _deleteWishList;
  BehaviorSubject<GetWishListResponse> get wishList => _getWishList;

  Future<LoginResponse> login(LoginRequest request) async {
    _login.value = LoginResponse();
    _login.value.loading = true;
    LoginResponse response = LoginResponse.fromMap((await repository.post(ApiRoutes.login(), request.toJson())).data);
    _login.value = response;
    _login.value.loading = false;
    return response;
  }

  Future<SignUpResponse> signup(SignUpRequest request) async {
    _signup.value = SignUpResponse();
    _signup.value.loading = true;
    SignUpResponse response = SignUpResponse.fromMap(await (await (repository.post(
      ApiRoutes.create_user(),
      request.toJson(),
      isForm: false,
    ))).data);

    _signup.value = response;
    _signup.value.loading = false;
    return response;
  }

  Future<CatResponse> getCat() async {
    _cats.value = CatResponse();
    _cats.value.loading = true ;
    CatResponse response = CatResponse.fromMap(await (await (repository.get(ApiRoutes.getMainCat()))). data);
    _cats.value = response;
    _cats.value.loading = false ;
    return response;
  }

  Future<SliderResponse> getSlider() async {
    _slider.value = SliderResponse();
    _slider.value.loading = true;
    SliderResponse response = SliderResponse.fromMap(await (await (repository.get(ApiRoutes.getSliders()))).data);
    _slider.value = response;
    _slider.value.loading = false ;
    return response;
  }

  Future<ProductsResponse> getProducts(int catID) async {
    _products.value = ProductsResponse();
    _products.value.loading = true ;
    ProductsResponse response = ProductsResponse.fromMap(await (await (repository.get(ApiRoutes.getProductsByCat(catID)))).data);
    log('--------------------->>> ${response.data}');
    _products.value = response;
    _products.value.loading = false;
    _products.value = _products.value;
    return response;
  }

  Future<SubCatResponse> getSubCats(int catID) async {
    _subCat.value = SubCatResponse();
    _subCat.value.loading = true ;
    SubCatResponse response = SubCatResponse.fromMap(await (await (repository.get(ApiRoutes.getSubCatByCat(catID)))).data);
    _subCat.value = response;
    _subCat.value.loading = false ;
    return response;
  }

  Future<ProducResponse> getProduct(int productID) async {
    _product.value = ProducResponse();
    _product.value.loading = true;
    ProducResponse response = ProducResponse.fromMap(await (await (repository.get(ApiRoutes.singleProduct(productID)))).data);
    _product.value = response;
    _product.value.loading = false;
    _product.value = _product.value;
    return response;
  }

  Future<BaseResponse> make_Order(MakeOrderRequest request) async {
    log('request ---> ${request.toJson()}');
    _makeOrder.value = BaseResponse();
    _makeOrder.value.loading = true ;
    BaseResponse response = BaseResponse.fromMap(await (await (repository.post(ApiRoutes.make_order(), request.toJson(), isForm: false))).data);
    _makeOrder.value = response;
    _makeOrder.value.loading = false ;
    return response;
  }

  Future<CoubonFoundResponse> check_coupone(String coupone) async {
    CoubonFoundResponse response = CoubonFoundResponse.fromJson(await (await (repository.get(ApiRoutes.check_coupon(coupone)))).data);
    return response;
  }

  Future<UserBean> getUserInfo(int userID) async {
    _userInfo.value = UserBean();
    Response response = await (repository.get(ApiRoutes.getUserInfo(userID)));
    UserBean userBean = UserBean.fromMap(response.data['data']);
    _userInfo.value = userBean;
    return userBean;
  }

  Future<ShoppingListResponse> getUserOrders() async {
    _userOrders.value = ShoppingListResponse();
    ShoppingListResponse response = ShoppingListResponse.fromJson(await (await (repository.get(ApiRoutes.getUserOrders()))).data);
    return response;
  }

  Future<ProductsResponse> searchFun(String search) async {
    _search.value = ProductsResponse();
    _search.value.loading = true ;
    ProductsResponse response = ProductsResponse.fromMap(await (await (repository.get(ApiRoutes.search(search)))).data);
    _search.value = response;
    _search.value.loading = false ;
    return response;
  }

  Future<ProductsDiscountRespones> getProductsDiscount() async {
    _productsDiscount.value = ProductsDiscountRespones();
    ProductsDiscountRespones response = ProductsDiscountRespones.fromJson(await (await (repository.get(ApiRoutes.getProductsDiscount()))).data);
    _productsDiscount.value = response;
    return response;
  }

  Future<BaseResponse> add_wishList(int productID) async {
    _addWishList.value = BaseResponse();
    _addWishList.value.loading = true ;
    BaseResponse response = BaseResponse.fromMap(await (await (repository.get(ApiRoutes.addWishlist(productID)))). data);
    _addWishList.value = response;
    _addWishList.value.loading = false ;
    return response;
  }

  Future<BaseResponse> delete_wish_list(int productID) async {
    _deleteWishList.value = BaseResponse();
    _deleteWishList.value.loading = true ;
    BaseResponse response = BaseResponse.fromMap(await (await (repository.get(ApiRoutes.deleteWishList(productID)))). data);
    _deleteWishList.value = response;
    _deleteWishList.value.loading = false ;
    return response;
  }

  Future<GetWishListResponse> getWishList() async {
    _getWishList.value = GetWishListResponse();
    _getWishList.value.loading = true ;
    GetWishListResponse response = GetWishListResponse.fromMap(await (await (repository.get(ApiRoutes.getWishlist()))). data);
    _getWishList.value = response;
    _getWishList.value.loading = false ;
    return response;
  }

  Future<Response> updateUserInfo(UpdateUserInfoRequest request) async {
    Response response = await (repository.post(ApiRoutes.updateUserInfo(), request.toJson()));
    return response;
  }

  void dispose() {
    _login.close();
    _signup.close();
    _search.close();
    _slider.close();
    _subCat.close();
    _addWishList.close();
    _deleteWishList.close();
    _cats.close();
    _product.close();
    _products.close();
    _productsDiscount.close();
    _userOrders.close();
    _makeOrder.close();
    _userInfo.close();
    _getWishList.close();
  }
}

final generalBloc = AuthBloc();