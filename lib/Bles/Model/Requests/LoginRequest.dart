import 'package:point_zero/utils/base/BaseRequest.dart';

class LoginRequest extends BaseRequest {
  String email;
  String password;

  LoginRequest({this.email, this.password});

  static LoginRequest fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    LoginRequest loginRequestBean = LoginRequest();
    loginRequestBean.email = map['email'];
    loginRequestBean.password = map['password'];
    return loginRequestBean;
  }

  Map<String, dynamic> toJson() => {
    "email": email,
    "password": password,
  };
}