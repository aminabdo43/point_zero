import 'dart:convert';

import 'package:point_zero/Bles/Model/Responses/cart_item_model.dart';

class MakeOrderRequest {
  List<CartItem> cart;
  String country;
  String coupon;
  String address;
  String paymentMethod;
  String mobile;
  int user;

  MakeOrderRequest({
    this.cart,
    this.country,
    this.coupon,
    this.address,
    this.paymentMethod,
    this.mobile,
    this.user,
  });

  Map<String, dynamic> toJson() => {
    "cart": cart.map((e) => json.encode(e.toJson())).toList().toString(),
    "country": country,
    "coupon": coupon,
    "address": address,
    "payment_method": paymentMethod,
    "mobile": mobile,
    "user": user,
  };
}