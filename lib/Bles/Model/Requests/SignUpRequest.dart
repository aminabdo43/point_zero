class SignUpRequest {
  String fname;
  String lname;
  String email;
  String mobile;
  String address;
  String password;

  SignUpRequest({
    this.fname,
    this.lname,
    this.email,
    this.mobile,
    this.address,
    this.password,
  });

  static SignUpRequest fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SignUpRequest signUpRequestBean = SignUpRequest();
    signUpRequestBean.fname = map['fname'];
    signUpRequestBean.lname = map['lname'];
    signUpRequestBean.email = map['email'];
    signUpRequestBean.mobile = map['mobile'];
    signUpRequestBean.address = map['address'];
    signUpRequestBean.password = map['password'];
    return signUpRequestBean;
  }

  Map<String, dynamic> toJson() => {
    "fname": fname,
    "lname": lname,
    "email": email,
    "mobile": mobile,
    "address": address,
    "password": password,
  };
}