class UpdateUserInfoRequest {
  String address1;
  String address2;
  String address3;
  String city;
  String country;
  String email;
  String fname;
  String lname;
  String mobile;

  UpdateUserInfoRequest(
      {this.address1,
      this.address2,
      this.address3,
      this.city,
      this.country,
      this.email,
      this.fname,
      this.lname,
      this.mobile});

  factory UpdateUserInfoRequest.fromJson(Map<String, dynamic> json) {
    return UpdateUserInfoRequest(
      address1: json['address1'],
      address2: json['address2'],
      address3: json['address3'],
      city: json['city'],
      country: json['country'],
      email: json['email'],
      fname: json['fname'],
      lname: json['lname'],
      mobile: json['mobile'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['address1'] = this.address1;
    data['address2'] = this.address2;
    data['address3'] = this.address3;
    data['city'] = this.city;
    data['country'] = this.country;
    data['email'] = this.email;
    data['fname'] = this.fname;
    data['lname'] = this.lname;
    data['mobile'] = this.mobile;
    return data;
  }
}
