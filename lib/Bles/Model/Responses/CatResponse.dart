import 'package:point_zero/utils/base/BaseResponse.dart';

class CatResponse extends BaseResponse {
  int status;
  String message;
  List<CatModel> data;

  static CatResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CatResponse catResponseBean = CatResponse();
    catResponseBean.status = map['status'];
    catResponseBean.message = map['message'];
    catResponseBean.data = []..addAll(
      (map['data'] as List ?? []).map((o) => CatModel.fromMap(o))
    );
    return catResponseBean;
  }

  Map toJson() => {
    "status": status,
    "message": message,
    "data": data,
  };
}

class CatModel {
  int id;
  String name;
  String image;
  int hasSubcats;

  static CatModel fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CatModel dataBean = CatModel();
    dataBean.id = map['id'];
    dataBean.name = map['name'];
    dataBean.image = map['image'];
    dataBean.hasSubcats = map['has_subcats'];
    return dataBean;
  }

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "image": image,
    "has_subcats": hasSubcats,
  };
}