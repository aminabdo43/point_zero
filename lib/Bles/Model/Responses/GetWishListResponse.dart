import 'package:point_zero/utils/base/BaseResponse.dart';

class GetWishListResponse extends BaseResponse {
  int status;
  String message;
  List<DataBean> data;

  static GetWishListResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    GetWishListResponse getWishListResponseBean = GetWishListResponse();
    getWishListResponseBean.status = map['status'];
    getWishListResponseBean.message = map['message'];
    getWishListResponseBean.data = List()..addAll(
      (map['data'] as List ?? []).map((o) => DataBean.fromMap(o))
    );
    return getWishListResponseBean;
  }

  Map toJson() => {
    "status": status,
    "message": message,
    "data": data,
  };
}

class DataBean {
  int id;
  int user;
  int product;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.id = map['id'];
    dataBean.user = map['user'];
    dataBean.product = map['product'];
    return dataBean;
  }

  Map toJson() => {
    "id": id,
    "user": user,
    "product": product,
  };
}