import 'dart:developer';

import 'package:point_zero/utils/base/BaseResponse.dart';

class LoginResponse extends BaseResponse {
  String msg;
  String token;
  UserBean user;

  static LoginResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    LoginResponse loginResponseBean = LoginResponse();
    loginResponseBean.msg = map['msg'];
    loginResponseBean.token = map['token'];
    loginResponseBean.user = UserBean.fromMap(map['user']);
    return loginResponseBean;
  }

  Map toJson() => {
    "msg": msg,
    "token": token,
    "user": user,
  };
}

class UserBean {
  int id;
  String fname;
  String lname;
  String email;
  String password;
  String mobile;
  String address1;
  String address2;
  String address3;
  String city;
  String country;
  String lastLogin;
  int banned;

  static UserBean fromMap(Map<String, dynamic> map) {
    log('--------> $map');
    if (map == null) return null;
    UserBean userBean = UserBean();
    userBean.id = map['id'];
    userBean.fname = map['fname'];
    userBean.lname = map['lname'];
    userBean.email = map['email'];
    userBean.password = map['password'];
    userBean.mobile = map['mobile'];
    userBean.address1 = map['address1'];
    userBean.address2 = map['address2'];
    userBean.address3 = map['address3'];
    userBean.city = map['city'];
    userBean.country = map['country'];
    userBean.lastLogin = map['last_login'];
    userBean.banned = map['banned'];
    return userBean;
  }

  Map toJson() => {
    "id": id,
    "fname": fname,
    "lname": lname,
    "email": email,
    "password": password,
    "mobile": mobile,
    "address1": address1,
    "address2": address2,
    "address3": address3,
    "city": city,
    "country": country,
    "last_login": lastLogin,
    "banned": banned,
  };
}