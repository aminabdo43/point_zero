import 'package:point_zero/utils/base/BaseResponse.dart';

class MakeOrderResponse extends BaseResponse {
  int status;
  String message;
  DataBean data;

  static MakeOrderResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    MakeOrderResponse makeOrderResponseBean = MakeOrderResponse();
    makeOrderResponseBean.status = map['status'];
    makeOrderResponseBean.message = map['message'];
    makeOrderResponseBean.data = DataBean.fromMap(map['data']);
    return makeOrderResponseBean;
  }

  Map toJson() => {
    "status": status,
    "message": message,
    "data": data,
  };
}

class DataBean {
  int orderId;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.orderId = map['order_id'];
    return dataBean;
  }

  Map toJson() => {
    "order_id": orderId,
  };
}