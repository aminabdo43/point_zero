import 'dart:convert';

import 'package:point_zero/utils/base/BaseResponse.dart';

class ProducResponse extends BaseResponse {
  int status;
  String message;
  ProductModel data;

  static ProducResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ProducResponse producResponseBean = ProducResponse();
    producResponseBean.status = map['status'];
    producResponseBean.message = map['message'];
    producResponseBean.data = ProductModel.fromMap(map['data']);
    return producResponseBean;
  }

  Map toJson() => {
    "status": status,
    "message": message,
    "data": data,
  };
}

class ProductModel {
  int id;
  String mainImage;
  String images;
  int catid;
  String name;
  String description;
  int price;
  int discount;
  int quantity;
  String details;
  String keywords;

  static ProductModel fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ProductModel dataBean = ProductModel();
    dataBean.id = map['id'];
    dataBean.mainImage = map['main_image'];
    dataBean.images = map['images'];
    dataBean.catid = map['catid'];
    dataBean.name = map['name'];
    dataBean.description = map['description'];
    dataBean.price = map['price'];
    dataBean.discount = map['discount'];
    dataBean.quantity = map['quantity'];
    map['details'] = map['details'].toString().replaceAll('\\', '');
    dataBean.details = map['details'];
    dataBean.details = map['details'];
    dataBean.keywords = map['keywords'];
    return dataBean;
  }

  Map<String, dynamic> toJson() => {
    "id": id,
    "main_image": mainImage,
    "images": images,
    "catid": catid,
    "name": name,
    "description": description,
    "price": price,
    "discount": discount,
    "quantity": quantity,
    "details": details,
    "keywords": keywords,
  };
}

class ProductDetails {
  String _type;
  String _name;
  List<String> _data;

  String get type => _type;
  String get name => _name;
  List<String> get data => _data;

  ProductDetails({
    String type,
    String name,
    List<String> data}){
    _type = type;
    _name = name;
    _data = data;
  }

  ProductDetails.fromJson(dynamic json) {
    _type = json["type"];
    _name = json["name"];
    _data = json["data"] != null ? json["data"].cast<String>() : [];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["type"] = _type;
    map["name"] = _name;
    map["data"] = _data;
    return map;
  }
}