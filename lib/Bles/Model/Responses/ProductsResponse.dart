import 'package:point_zero/utils/base/BaseResponse.dart';

import 'ProducResponse.dart';

class ProductsResponse extends BaseResponse {
  int status;
  String message;
  List<ProductModel> data;

  static ProductsResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ProductsResponse productsResponseBean = ProductsResponse();
    productsResponseBean.status = map['status'];
    productsResponseBean.message = map['message'];
    productsResponseBean.data = List()..addAll(
      (map['data'] as List ?? []).map((o) => ProductModel.fromMap(o))
    );
    return productsResponseBean;
  }

  Map toJson() => {
    "status": status,
    "message": message,
    "data": data,
  };
}