import 'package:point_zero/utils/base/BaseResponse.dart';

class SignUpResponse extends BaseResponse {
  String msg;

  static SignUpResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SignUpResponse signUpResponseBean = SignUpResponse();
    signUpResponseBean.msg = map['msg'];
    return signUpResponseBean;
  }

  Map<String, dynamic> toJson() => {
    "msg": msg,
  };
}