import 'package:point_zero/utils/base/BaseResponse.dart';

class SliderResponse extends BaseResponse {
  int status;
  String message;
  List<SliderModel> data;

  static SliderResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SliderResponse sliderResponseBean = SliderResponse();
    sliderResponseBean.status = map['status'];
    sliderResponseBean.message = map['message'];
    sliderResponseBean.data = List()..addAll(
      (map['data'] as List ?? []).map((o) => SliderModel.fromMap(o))
    );
    return sliderResponseBean;
  }

  Map toJson() => {
    "status": status,
    "message": message,
    "data": data,
  };
}

class SliderModel {
  int id;
  String name;
  List<ProductsBean> products;

  static SliderModel fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SliderModel dataBean = SliderModel();
    dataBean.id = map['id'];
    dataBean.name = map['name'];
    dataBean.products = []..addAll(
      (map['products'] as List ?? []).map((o) => ProductsBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "id": id,
    "name": name,
    "products": products,
  };
}

class ProductsBean {
  int id;
  String mainImage;
  String images;
  int catid;
  String name;
  String description;
  int price;
  int discount;
  int quantity;
  String details;
  String keywords;

  static ProductsBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ProductsBean productsBean = ProductsBean();
    productsBean.id = map['id'];
    productsBean.mainImage = map['main_image'];
    productsBean.images = map['images'];
    productsBean.catid = map['catid'];
    productsBean.name = map['name'];
    productsBean.description = map['description'];
    productsBean.price = map['price'];
    productsBean.discount = map['discount'];
    productsBean.quantity = map['quantity'];
    productsBean.details = map['details'];
    productsBean.keywords = map['keywords'];
    return productsBean;
  }

  Map<String, dynamic> toJson() => {
    "id": id,
    "main_image": mainImage,
    "images": images,
    "catid": catid,
    "name": name,
    "description": description,
    "price": price,
    "discount": discount,
    "quantity": quantity,
    "details": details,
    "keywords": keywords,
  };
}

