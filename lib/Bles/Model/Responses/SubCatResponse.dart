import 'package:point_zero/utils/base/BaseResponse.dart';

class SubCatResponse extends BaseResponse {
  int status;
  String message;
  List<SubCatModel> data;

  static SubCatResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SubCatResponse subCatResponseBean = SubCatResponse();
    subCatResponseBean.status = map['status'];
    subCatResponseBean.message = map['message'];
    subCatResponseBean.data = []..addAll(
      (map['data'] as List ?? []).map((o) => SubCatModel.fromMap(o))
    );
    return subCatResponseBean;
  }

  Map toJson() => {
    "status": status,
    "message": message,
    "data": data,
  };
}

class SubCatModel {
  String sname;
  int id;
  int category;
  String name;

  static SubCatModel fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SubCatModel dataBean = SubCatModel();
    dataBean.sname = map['sname'];
    dataBean.id = map['id'];
    dataBean.category = map['category'];
    dataBean.name = map['name'];
    return dataBean;
  }

  Map toJson() => {
    "sname": sname,
    "id": id,
    "category": category,
    "name": name,
  };
}