
import 'package:equatable/equatable.dart';

class CartItem extends Equatable {
  String size;
  String color;
  String id;
  int quantity;
  String image;
  String name;
  String price;
  String orderNumber;

  CartItem({
    this.size = 'XL',
    this.color = 'green',
    this.id,
    this.quantity = 1,
    this.image,
    this.name,
    this.price,
    this.orderNumber,
  });

  CartItem.fromJson(Map<String, dynamic> json) {
    size = json['size'];
    color = json['color'];
    id = json['id'];
    quantity = json['quantity'];
  }

  CartItem.fromJsonSaving(Map<String, dynamic> json) {
    size = json['size'];
    color = json['color'];
    id = json['id'];
    quantity = json['quantity'];
    orderNumber = json['orderNumber'];
    image = json['image'];
    price = json['price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['size'] = this.size;
    data['color'] = this.color;
    data['id'] = this.id;
    data['quantity'] = this.quantity;
    return data;
  }

  Map<String, dynamic> toJsonForSaving() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['size'] = this.size;
    data['color'] = this.color;
    data['id'] = this.id;
    data['quantity'] = this.quantity;
    data['image'] = this.image;
    data['price'] = this.price;
    data['orderNumber'] = this.orderNumber;
    return data;
  }

  @override
  List<Object> get props => [this.id, this.color, this.size, this.quantity, this.name];

  @override
  String toString() {
    return 'CartItem{size: $size, color: $color, id: $id, quantity: $quantity, image: $image, name: $name, price: $price, orderNumber: $orderNumber}';
  }
}