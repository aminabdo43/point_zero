
class CoubonFoundResponse {
  int _status;
  String _message;
  List<Data> _data;

  int get status => _status;
  String get message => _message;
  List<Data> get data => _data;

  CoubonFoundResponse({
      int status, 
      String message, 
      List<Data> data}){
    _status = status;
    _message = message;
    _data = data;
}

  CoubonFoundResponse.fromJson(dynamic json) {
    _status = json["status"];
    _message = json["message"];
    if (json["data"] != null) {
      _data = [];
      json["data"].forEach((v) {
        _data.add(Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status"] = _status;
    map["message"] = _message;
    if (_data != null) {
      map["data"] = _data.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : 9
/// name : "ABC"
/// amount : 10

class Data {
  int _id;
  String _name;
  int _amount;

  int get id => _id;
  String get name => _name;
  int get amount => _amount;

  Data({
      int id, 
      String name, 
      int amount}){
    _id = id;
    _name = name;
    _amount = amount;
}

  Data.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _amount = json["amount"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["amount"] = _amount;
    return map;
  }

}