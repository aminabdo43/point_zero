
class ProductsDiscountRespones {
  int _status;
  String _message;
  List<Data> _data;

  int get status => _status;
  String get message => _message;
  List<Data> get data => _data;

  ProductsDiscountRespones({
      int status, 
      String message, 
      List<Data> data}){
    _status = status;
    _message = message;
    _data = data;
}

  ProductsDiscountRespones.fromJson(dynamic json) {
    _status = json["status"];
    _message = json["message"];
    if (json["data"] != null) {
      _data = [];
      json["data"].forEach((v) {
        _data.add(Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status"] = _status;
    map["message"] = _message;
    if (_data != null) {
      map["data"] = _data.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : 25
/// main_image : "http://pointzerops.com/files/uploads/16152014422080ab63062f1fae4a8ae3437df3fed4255.jpg"
/// images : "http://pointzerops.com/files/uploads/1615201442208iPhone X, XS, 11 Pro – 1.png,http://pointzerops.com/files/uploads/1615201442208iPhone X, XS, 11 Pro – 2.png,http://pointzerops.com/files/uploads/1615201442208iPhone X, XS, 11 Pro – 3.png,http://pointzerops.com/files/uploads/1615201442208iPhone X, XS, 11 Pro – 4.png,http://pointzerops.com/files/uploads/1615201442208iPhone X, XS, 11 Pro – 5.png"
/// catid : 10020
/// name : "بدلة رياضة"
/// description : "فساتين سهرة جديدة ذات رقبة مفتوحة ومكشوفة الكتف من الساتان المخملي لعام 2021"
/// price : 100
/// discount : 5
/// quantity : -4
/// details : "[{\"type\":\"compo\",\"name\":\"Color\",\"data\":[\"أحمر\",\"أخضر\",\"أسود\"]},{\"type\":\"compo\",\"name\":\"Size\",\"data\":[\"S\",\"M\",\"L\"]}]"
/// keywords : "فستان,حفلة,مكشوف"

class Data {
  int _id;
  String _mainImage;
  String _images;
  int _catid;
  String _name;
  String _description;
  int _price;
  int _discount;
  int _quantity;
  String _details;
  String _keywords;

  int get id => _id;
  String get mainImage => _mainImage;
  String get images => _images;
  int get catid => _catid;
  String get name => _name;
  String get description => _description;
  int get price => _price;
  int get discount => _discount;
  int get quantity => _quantity;
  String get details => _details;
  String get keywords => _keywords;

  Data({
      int id, 
      String mainImage, 
      String images, 
      int catid, 
      String name, 
      String description, 
      int price, 
      int discount, 
      int quantity, 
      String details, 
      String keywords}){
    _id = id;
    _mainImage = mainImage;
    _images = images;
    _catid = catid;
    _name = name;
    _description = description;
    _price = price;
    _discount = discount;
    _quantity = quantity;
    _details = details;
    _keywords = keywords;
}

  Data.fromJson(dynamic json) {
    _id = json["id"];
    _mainImage = json["main_image"];
    _images = json["images"];
    _catid = json["catid"];
    _name = json["name"];
    _description = json["description"];
    _price = json["price"];
    _discount = json["discount"];
    _quantity = json["quantity"];
    _details = json["details"];
    _keywords = json["keywords"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["main_image"] = _mainImage;
    map["images"] = _images;
    map["catid"] = _catid;
    map["name"] = _name;
    map["description"] = _description;
    map["price"] = _price;
    map["discount"] = _discount;
    map["quantity"] = _quantity;
    map["details"] = _details;
    map["keywords"] = _keywords;
    return map;
  }

}