class ShoppingListResponse {
    List<Data> data;
    String message;
    int status;

    ShoppingListResponse({this.data, this.message, this.status});

    factory ShoppingListResponse.fromJson(Map<String, dynamic> json) {
        return ShoppingListResponse(
            data: json['data'] != null ? (json['data'] as List).map((i) => Data.fromJson(i)).toList() : null, 
            message: json['message'], 
            status: json['status'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['message'] = this.message;
        data['status'] = this.status;
        if (this.data != null) {
            data['data'] = this.data.map((v) => v.toJson()).toList();
        }
        return data;
    }
}

class Data {
    String address;
    String cart;
    String coupon;
    String date_added;
    int delivery_price;
    int id;
    int mobile;
    String payment_method;
    int status;
    int total_price;
    int user;

    Data({this.address, this.cart, this.coupon, this.date_added, this.delivery_price, this.id, this.mobile, this.payment_method, this.status, this.total_price, this.user});

    factory Data.fromJson(Map<String, dynamic> json) {
        return Data(
            address: json['address'], 
            cart: json['cart'], 
            coupon: json['coupon'], 
            date_added: json['date_added'], 
            delivery_price: json['delivery_price'], 
            id: json['id'], 
            mobile: json['mobile'], 
            payment_method: json['payment_method'], 
            status: json['status'], 
            total_price: json['total_price'], 
            user: json['user'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['address'] = this.address;
        data['cart'] = this.cart;
        data['coupon'] = this.coupon;
        data['date_added'] = this.date_added;
        data['delivery_price'] = this.delivery_price;
        data['id'] = this.id;
        data['mobile'] = this.mobile;
        data['payment_method'] = this.payment_method;
        data['status'] = this.status;
        data['total_price'] = this.total_price;
        data['user'] = this.user;
        return data;
    }
}