/// status : 200
/// message : "Found"
/// data : [{"id":5,"user":16,"product":27}]

class WishlistResponse {
  int _status;
  String _message;
  List<WishlistItem> _data;

  int get status => _status;

  String get message => _message;

  List<WishlistItem> get data => _data;

  WishlistResponse({int status, String message, List<WishlistItem> data}) {
    _status = status;
    _message = message;
    _data = data;
  }

  WishlistResponse.fromJson(Map<String, dynamic> json) {
    _status = json["status"];
    _message = json["message"];
    if (json["data"] != null) {
      _data = [];
      json["data"].forEach((v) {
        _data.add(WishlistItem.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status"] = _status;
    map["message"] = _message;
    if (_data != null) {
      map["data"] = _data.map((v) => v.toJson()).toList();
    }
    return map;
  }
}


class WishlistItem {
  int _id;
  int _user;
  int _product;

  int get id => _id;

  int get user => _user;

  int get product => _product;

  WishlistItem({int id, int user, int product}) {
    _id = id;
    _user = user;
    _product = product;
  }

  WishlistItem.fromJson(dynamic json) {
    _id = json["id"];
    _user = json["user"];
    _product = json["product"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["user"] = _user;
    map["product"] = _product;
    return map;
  }
}
