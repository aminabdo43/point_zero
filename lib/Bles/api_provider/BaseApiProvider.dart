import 'package:dio/dio.dart';
import 'package:point_zero/utils/app_utils.dart';
import 'package:point_zero/utils/base/BaseResponse.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../ApiRoute.dart';

SharedPreferences pref;

Options get options => Options(
      followRedirects: false,
      validateStatus: (status) {
        return status < 500;
      },
      headers: {
        'Authorization': AppUtils.userData == null ? '' : 'Bearer ${AppUtils.userData.token}',
        'lang': pref.getString('langCode') ?? 'ar'
      },
    );

class BaseApiProvider {
  String baseUrl = "";
  String token = "";

  Dio dio = Dio();

  FormData formData;

  Future<Response> generalGet(String urlExtension) async {
    try {
      pref = await SharedPreferences.getInstance();
      if (urlExtension.contains("product_details")) {
        Response response = await dio.get(
          ApiRoutesUpdate().getLink(ApiRoutes.generalGet(urlExtension)),
          options: options,
        );
        print("response ----- >");
        print(response);
        return response;
      } else {
        Response response = await dio.get(
          ApiRoutesUpdate().getLink(ApiRoutes.generalGet(urlExtension)),
          options: options,
        );
        print("response ----- >");
        print(response);
        return response;
      }
    } catch (error, stacktrace) {
      print("response 000 ");
      print("Exception occured: $error stackTrace: $stacktrace");
      Response response = new Response(
          statusMessage: "no internet connection",
          statusCode: 404,
          data: BaseResponse(loading: false, message: "connection error", status: 0));
      AppUtils.showToast(msg: "Connection Error with Network");
      return response;
    }
  }

  Future<Response> generalDelete(String urlExtention) async {
    try {
      pref = await SharedPreferences.getInstance();
      Response response = await dio.delete(
          ApiRoutesUpdate().getLink(ApiRoutes.generalGet(urlExtention)),
          options: options);
      print("response ----- >");
      print(response);
      return response;
    } catch (error, stacktrace) {
      print("response 000 ");
      print("Exception occured: $error stackTrace: $stacktrace");
      return null;
    }
  }

  Future<Response> generalPost(String urlExtention, request, {bool isForm = false}) async {
    try {
      pref = await SharedPreferences.getInstance();
      Response response = await dio.post(
          ApiRoutesUpdate().getLink(ApiRoutes.generalGet(urlExtention)),
          options: options,
          data: isForm ? FormData.fromMap(request) : request);
      print("response ----- >");
      print(response);
      return response;
    } catch (error, stacktrace) {
      print("response 000 ");
      print("Exception occured: $error stackTrace: $stacktrace");
      return null;
    }
  }
}