import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:point_zero/pages/auth/login_page.dart';
import 'package:point_zero/pages/auth/signup_page.dart';
import 'package:point_zero/pages/home/home_page.dart';
import 'package:point_zero/pages/payment/step_indicator_provider.dart';
import 'package:point_zero/utils/app_utils.dart';
import 'package:point_zero/utils/consts.dart';
import 'package:point_zero/utils/pointers.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'pages/splash/splash_page.dart';
import 'providers/page_provider.dart';
import 'utils/app_localization.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  AppUtils.userData = await AppUtils.getUserData();

  Pointers.cartItems = await Pointers.getStoredItems();

  runApp(
    MyApp(
      languageCode: (await SharedPreferences.getInstance()).getString('langCode') ?? 'ar',
    ),
  );
}

class MyApp extends StatefulWidget {
  final String languageCode;

  const MyApp({Key key, this.languageCode}) : super(key: key);

  static void setLocale(BuildContext context, Locale locale) {
    _MyAppState state = context.findAncestorStateOfType<_MyAppState>();

    state.setState(() {
      state._locale = locale;
    });
  }

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Locale _locale;

  @override
  void initState() {
    super.initState();

    setLanguage();
  }

  setLanguage() async {
    _locale = Locale(widget.languageCode);
    if (mounted) setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => PageProvider(),),
        ChangeNotifierProvider(create: (_) => StepIndicatorProvider(),),
      ],
      child: MaterialApp(
        title: appName,
        debugShowCheckedModeBanner: false,
        locale: _locale,
        localizationsDelegates: [
          AppLocalizationsDelegate(),
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale('en'), // English
          const Locale('ar'), // Arabic
        ],
        theme: ThemeData(
          scaffoldBackgroundColor: Colors.white,
          primarySwatch: Colors.pink,
          visualDensity: VisualDensity.adaptivePlatformDensity,
          fontFamily: 'cairo',
        ),
        home: Builder(
          builder: (context) => Directionality(
            child: SplashPage(),
            textDirection: Localizations.localeOf(context).languageCode == 'ar'
                ? TextDirection.rtl
                : TextDirection.ltr,
          ),
        ),
      ),
    );
  }
}
