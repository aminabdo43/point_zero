
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:point_zero/pages/account/address_page.dart';
import 'package:point_zero/pages/account/my_shopping_page.dart';
import 'package:point_zero/pages/account/notifications_page.dart';
import 'package:point_zero/pages/account/wallet_page.dart';
import 'package:point_zero/pages/filter/filter_page.dart';
import 'package:point_zero/pages/home/home_contents.dart';
import 'package:point_zero/pages/splash/splash_page.dart';
import 'package:point_zero/providers/page_provider.dart';
import 'package:point_zero/utils/app_utils.dart';
import 'package:point_zero/utils/consts.dart';
import 'package:point_zero/widgets/my_app_bar.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'contact_us_page.dart';
import 'my_information_page.dart';

class AccountPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: () async {
        Provider.of<PageProvider>(context, listen: false).setPage(0, HomeContents());
        return false;
      },
      child: Scaffold(
        body: Stack(
          children: [
            Container(
              width: size.width,
              height: size.height,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: Image.asset(
                    'assets/images/fav_bg.png',
                    width: size.width,
                    height: size.height,
                  ).image,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Container(
              width: size.width,
              height: size.height,
              color: mainColor.withOpacity(.35),
            ),
            Padding(
              padding: EdgeInsets.only(
                left: 14,
                right: 14,
                top: MediaQuery.of(context).padding.top + 10,
              ),
              child: Container(
                width: size.width,
                height: size.height,
                decoration: BoxDecoration(
                  color: Colors.white,
                  gradient: LinearGradient(
                    begin: Alignment.bottomCenter,
                    end: Alignment.topCenter,
                    colors: [
                      Color(0xffFAFAF9),
                      Color(0xffFDFCF9),
                      Color(0xffF9EFEA),
                    ],
                  ),
                  border: Border.all(color: mainColor),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30),
                  ),
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30),
                  ),
                  child: SingleChildScrollView(
                    physics: bouncingScrollPhysics,
                    child: Column(
                      children: [
                        MyAppBar(title: AppUtils.translate(context, 'my_account'), leadingIcon: SizedBox.shrink(),),
                        // SizedBox(
                        //   height: 20,
                        // ),
                        // CircleAvatar(
                        //   radius: 65,
                        //   backgroundImage: Image.asset('assets/images/boy.png', fit: BoxFit.fill,).image,
                        // ),
                        // SizedBox(
                        //   height: size.height * .05,
                        // ),
                        // Divider(thickness: 1,),
                        ListTile(
                          leading: Image.asset('assets/images/account.png'),
                          title: Text(AppUtils.translate(context, 'my_info')),
                          trailing: Icon(Localizations.localeOf(context).languageCode == 'en' ? Icons.arrow_back_ios_rounded : Icons.arrow_forward_ios_rounded, size: 20,),
                          onTap: () {
                            Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: MyInformationPage()));
                          },
                        ),
                        Divider(thickness: 1,),
                        // ListTile(
                        //   leading: Icon(Icons.filter_list),
                        //   title: Text(AppUtils.translate(context, 'filter')),
                        //   trailing: Icon(Localizations.localeOf(context).languageCode == 'en' ? Icons.arrow_back_ios_rounded : Icons.arrow_forward_ios_rounded, size: 20,),
                        //   onTap: () {
                        //     Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: FilterPage()));
                        //   },
                        // ),
                        // Divider(thickness: 1,),
                        // ListTile(
                        //   leading: Image.asset('assets/images/location.png'),
                        //   title: Text(AppUtils.translate(context, 'address')),
                        //   trailing: Icon(Localizations.localeOf(context).languageCode == 'en' ? Icons.arrow_back_ios_rounded : Icons.arrow_forward_ios_rounded, size: 20,),
                        //   onTap: () {
                        //     Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: AddressPage()));
                        //   },
                        // ),
                        // Divider(thickness: 1,),
                        // ListTile(
                        //   leading: Image.asset('assets/images/currency.png'),
                        //   title: Text(AppUtils.translate(context, 'wallet')),
                        //   trailing: Icon(Localizations.localeOf(context).languageCode == 'en' ? Icons.arrow_back_ios_rounded : Icons.arrow_forward_ios_rounded, size: 20,),
                        //   onTap: () {
                        //     Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: WalletPage()));
                        //   },
                        // ),
                        // Divider(thickness: 1,),
                        ListTile(
                          leading: Image.asset('assets/images/finished.png'),
                          title: Text(AppUtils.translate(context, 'shopping')),
                          trailing: Icon(Localizations.localeOf(context).languageCode == 'en' ? Icons.arrow_back_ios_rounded : Icons.arrow_forward_ios_rounded, size: 20,),
                          onTap: () {
                            Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: MyShoppingPage()));
                          },
                        ),
                        Divider(thickness: 1,),
                        // ListTile(
                        //   leading: Image.asset('assets/images/notifications.png'),
                        //   title: Text(AppUtils.translate(context, 'notifications')),
                        //   trailing: Icon(Localizations.localeOf(context).languageCode == 'en' ? Icons.arrow_back_ios_rounded : Icons.arrow_forward_ios_rounded, size: 20,),
                        //   onTap: () {
                        //     Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: NotificationsPage()));
                        //   },
                        // ),
                        // Divider(thickness: 1,),
                        // ListTile(
                        //   leading: Image.asset('assets/images/settings.png'),
                        //   title: Text(AppUtils.translate(context, 'contact_us')),
                        //   trailing: Icon(Localizations.localeOf(context).languageCode == 'en' ? Icons.arrow_back_ios_rounded : Icons.arrow_forward_ios_rounded, size: 20,),
                        //   onTap: () {
                        //     Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: ContactUsPage()));
                        //   },
                        // ),
                        // Divider(thickness: 1,),
                        ListTile(
                          leading: Image.asset('assets/images/logout.png'),
                          title: Text(AppUtils.translate(context, 'logout')),
                          trailing: Icon(Localizations.localeOf(context).languageCode == 'en' ? Icons.arrow_back_ios_rounded : Icons.arrow_forward_ios_rounded, size: 20,),
                          onTap: () async {
                            AppUtils.userData = null;
                            await SharedPreferences.getInstance().then((value) => value.clear());
                            Navigator.pushAndRemoveUntil(context, PageTransition(type: PageTransitionType.rightToLeft, child: SplashPage()), (_) => false).then((value) {});
                          },
                        ),
                        Divider(
                          thickness: 1,
                        ),
                        SizedBox(
                          height: 60,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

