import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:point_zero/utils/app_utils.dart';
import 'package:point_zero/utils/consts.dart';
import 'package:point_zero/widgets/my_app_bar.dart';
import 'package:point_zero/widgets/my_button.dart';

class AddressPage extends StatefulWidget {
  @override
  _AddressPageState createState() => _AddressPageState();
}

class _AddressPageState extends State<AddressPage> {
  int paymentType = 1;

  FocusNode str1Node = FocusNode();
  FocusNode str2Node = FocusNode();
  FocusNode elhayNode = FocusNode();
  FocusNode cityNode = FocusNode();
  FocusNode countryNode = FocusNode();

  bool sameAddress = false;

  List<String> countries = [
    'الضفة الغربية',
    "القدس",
    "اراضي 48",
  ];

  String selectedCountry;

  @override
  void initState() {
    super.initState();

    selectedCountry = countries[0];
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: [
          Container(
            width: size.width,
            height: size.height,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: Image.asset(
                  'assets/images/fav_bg.png',
                  width: size.width,
                  height: size.height,
                ).image,
                fit: BoxFit.cover,
              ),
            ),
          ),
          Container(
            width: size.width,
            height: size.height,
            color: mainColor.withOpacity(.35),
          ),
          Padding(
            padding: EdgeInsets.only(
              left: 14,
              right: 14,
              top: MediaQuery.of(context).padding.top + 10,
            ),
            child: Container(
              width: size.width,
              decoration: BoxDecoration(
                color: Colors.white,
                gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [
                    Color(0xffFAFAF9),
                    Color(0xffFDFCF9),
                    Color(0xffF9EFEA),
                  ],
                ),
                border: Border.all(color: mainColor),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                ),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                ),
                child: SingleChildScrollView(
                  physics: bouncingScrollPhysics,
                  child: Column(
                    children: [
                      MyAppBar(
                        title: AppUtils.translate(context, 'address'),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 20,
                            ),
                            Text(AppUtils.translate(context, 'country')),
                            Container(
                              width: double.infinity,
                              padding: EdgeInsets.only(top: 14),
                              child: DropdownButton<String>(
                                isExpanded: true,
                                elevation: 4,
                                value: selectedCountry,
                                underline: Container(width: double.infinity, height: 1.5, color: Colors.grey,),
                                items: countries.map((String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Container(
                                        child: Text(value),
                                        alignment: Localizations.localeOf(context).languageCode == 'ar' ? Alignment.centerRight : Alignment.centerLeft,
                                    ),
                                  );
                                }).toList(),
                                onChanged: (newValue) {
                                  setState(() {
                                    selectedCountry = newValue;
                                  });
                                },
                              ),
                            ),
                            SizedBox(
                              height: 40,
                            ),
                            ListTile(
                              onTap: () {
                                setState(() {
                                  sameAddress = !sameAddress;
                                });
                              },
                              leading: Container(
                                width: 30,
                                height: 30,
                                decoration: BoxDecoration(
                                  color: mainColor,
                                  shape: BoxShape.circle,
                                ),
                                child: sameAddress
                                    ? Icon(
                                        Icons.check,
                                        color: Colors.white,
                                        size: 18,
                                      )
                                    : SizedBox.shrink(),
                              ),
                              title: Text(
                                AppUtils.translate(
                                    context, 'payment_address_msg'),
                              ),
                            ),
                            SizedBox(
                              height: 40,
                            ),
                            Center(
                              child: myButton(
                                AppUtils.translate(context, 'save'),
                                context,
                                onTap: () {},
                              ),
                            ),
                            SizedBox(
                              height: 40,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
