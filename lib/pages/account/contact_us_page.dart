import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:point_zero/utils/app_utils.dart';
import 'package:point_zero/utils/consts.dart';
import 'package:point_zero/widgets/my_app_bar.dart';

class ContactUsPage extends StatefulWidget {
  @override
  _ContactUsPageState createState() => _ContactUsPageState();
}

class _ContactUsPageState extends State<ContactUsPage> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: [
          Container(
            width: size.width,
            height: size.height,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: Image.asset(
                  'assets/images/fav_bg.png',
                  width: size.width,
                  height: size.height,
                ).image,
                fit: BoxFit.cover,
              ),
            ),
          ),
          Container(
            width: size.width,
            height: size.height,
            color: mainColor.withOpacity(.35),
          ),
          Padding(
            padding: EdgeInsets.only(
              left: 14,
              right: 14,
              top: MediaQuery.of(context).padding.top + 10,
            ),
            child: Container(
              width: size.width,
              decoration: BoxDecoration(
                color: Colors.white,
                gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [
                    Color(0xffFAFAF9),
                    Color(0xffFDFCF9),
                    Color(0xffF9EFEA),
                  ],
                ),
                border: Border.all(color: mainColor),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                ),
              ),
              child: Column(
                children: [
                  MyAppBar(
                    title: AppUtils.translate(context, 'contact_us'),
                  ),
                  ListTile(
                    title: Text('Facebook'),
                    leading: Icon(
                      FontAwesomeIcons.facebookSquare,
                      color: Color(0xff3B89D7),
                      size: 25,
                    ),
                    onTap: () {},
                  ),
                  Divider(
                    thickness: 1,
                  ),
                  ListTile(
                    title: Text('Instagram'),
                    leading: Icon(
                      FontAwesomeIcons.instagram,
                      size: 25,
                      color: Color(0xffCB3D86),
                    ),
                    onTap: () {},
                  ),
                  Divider(
                    thickness: 1,
                  ),
                  ListTile(
                    title: Text('Whatsapp'),
                    leading: Icon(
                      FontAwesomeIcons.whatsappSquare,
                      size: 25,
                      color: Color(0xff3CDC5C),
                    ),
                    onTap: () {},
                  ),
                  Divider(
                    thickness: 1,
                  ),
                  Expanded(
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            AppUtils.translate(context, 'designed_by'),
                            style: TextStyle(),
                          ),
                          Text(
                            'Abanoub',
                            style: TextStyle(color: mainColor, fontSize: 12),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: size.height * .05,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
