import 'package:flutter/material.dart';
import 'package:point_zero/utils/consts.dart';

class ExpandedItem extends StatefulWidget {
  final String title;
  final Widget child;

  const ExpandedItem({Key key, @required this.title, this.child})
      : super(key: key);

  @override
  _ExpandedItemState createState() => _ExpandedItemState();
}

class _ExpandedItemState extends State<ExpandedItem> {
  bool expand = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          trailing: Icon(
            expand ? Icons.keyboard_arrow_up : Icons.keyboard_arrow_down,
            color: expand ? mainColor : Colors.black,
          ),
          title: Text(widget.title),
          onTap: () {
            setState(() {
              expand = !expand;
            });
          },
        ),
        expand ? widget.child : null,
      ].where((element) => element != null).toList(),
    );
  }
}
