
import 'package:flutter/material.dart';
import 'package:point_zero/Bles/Bloc/AuthBloc.dart';
import 'package:point_zero/Bles/Model/Requests/update_user_info_request.dart';
import 'package:point_zero/Bles/Model/Responses/LoginResponse.dart';
import 'package:point_zero/pages/account/expand_item.dart';
import 'package:point_zero/utils/app_utils.dart';
import 'package:point_zero/utils/consts.dart';
import 'package:point_zero/widgets/my_app_bar.dart';
import 'package:point_zero/widgets/my_button.dart';
import 'package:point_zero/widgets/my_loader.dart';
import 'package:point_zero/widgets/my_text_form_field.dart';

class MyInformationPage extends StatefulWidget {

  @override
  _MyInformationPageState createState() => _MyInformationPageState();
}

class _MyInformationPageState extends State<MyInformationPage> {

  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController countryController = TextEditingController();
  TextEditingController cityController = TextEditingController();

  bool gotData = false;
  bool updating = false;

  List<String> countries = ['الضفة الغربية', 'أراضي 48', 'القدس'];

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: [
          Container(
            width: size.width,
            height: size.height,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: Image.asset(
                  'assets/images/fav_bg.png',
                  width: size.width,
                  height: size.height,
                ).image,
                fit: BoxFit.cover,
              ),
            ),
          ),
          Container(
            width: size.width,
            height: size.height,
            color: mainColor.withOpacity(.35),
          ),
          Padding(
            padding: EdgeInsets.only(
              left: 14,
              right: 14,
              top: MediaQuery.of(context).padding.top + 10,
            ),
            child: Container(
              width: size.width,
              height: double.infinity,
              decoration: BoxDecoration(
                color: Colors.white,
                gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [
                    Color(0xffFAFAF9),
                    Color(0xffFDFCF9),
                    Color(0xffF9EFEA),
                  ],
                ),
                border: Border.all(color: mainColor),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                ),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                ),
                child: SingleChildScrollView(
                  physics: bouncingScrollPhysics,
                  child: Column(
                    children: [
                      MyAppBar(title: AppUtils.translate(context, 'my_info')),
                      SizedBox(
                        height: size.height * .05,
                      ),
                      FutureBuilder<UserBean>(
                        future: generalBloc.getUserInfo(AppUtils.userData?.userBean?.id),
                         builder: (context, snapshot) {
                           if(snapshot.hasData) {
                             if(!gotData) {
                               gotData = true;
                               firstNameController.text = snapshot.data.fname ?? '';
                               lastNameController.text = snapshot.data.lname ?? '';
                               emailController.text = snapshot.data.email ?? '';
                               addressController.text = snapshot.data.address1 ?? '';
                               countryController.text = snapshot.data.country ?? '';
                               cityController.text = snapshot.data.city ?? '';
                             }
                             return Column(
                               children: [
                                 ExpandedItem(
                                   title: AppUtils.translate(context, 'name_and_title'),
                                   child: Padding(
                                     padding: EdgeInsets.symmetric(horizontal: 20),
                                     child: Column(
                                       crossAxisAlignment: CrossAxisAlignment.start,
                                       children: [
                                         MyTextFormField(
                                           hintText: AppUtils.translate(context, 'first_name'),
                                           hintStyle: TextStyle(fontSize: 12, color: greyColor),
                                           controller: firstNameController,
                                         ),
                                         SizedBox(
                                           height: size.height * .02,
                                         ),
                                         MyTextFormField(
                                           hintText: AppUtils.translate(context, 'last_name'),
                                           hintStyle: TextStyle(fontSize: 12, color: greyColor),
                                           controller: lastNameController,
                                         ),
                                         SizedBox(
                                           height: size.height * .05,
                                         ),
                                       ],
                                     ),
                                   ),
                                 ),
                                 ExpandedItem(
                                   title: AppUtils.translate(context, 'email'),
                                   child: Padding(
                                     padding: EdgeInsets.symmetric(horizontal: 20),
                                     child: Column(
                                       crossAxisAlignment: CrossAxisAlignment.start,
                                       children: [
                                         MyTextFormField(
                                           hintText: AppUtils.translate(context, 'email'),
                                           hintStyle: TextStyle(fontSize: 12, color: greyColor),
                                           controller: emailController,
                                         ),
                                         SizedBox(
                                           height: size.height * .05,
                                         ),
                                       ],
                                     ),
                                   ),
                                 ),
                                 ExpandedItem(
                                   title: AppUtils.translate(context, 'address'),
                                   child: Padding(
                                     padding: EdgeInsets.symmetric(horizontal: 20),
                                     child: Column(
                                       crossAxisAlignment: CrossAxisAlignment.start,
                                       children: [
                                         MyTextFormField(
                                           hintText: AppUtils.translate(context, 'address'),
                                           hintStyle: TextStyle(fontSize: 12, color: greyColor),
                                           controller: addressController,
                                         ),
                                         SizedBox(
                                           height: size.height * .05,
                                         ),
                                       ],
                                     ),
                                   ),
                                 ),
                                 ExpandedItem(
                                   title: AppUtils.translate(context, 'country'),
                                   child: Padding(
                                     padding: EdgeInsets.symmetric(horizontal: 20),
                                     child: Column(
                                       crossAxisAlignment: CrossAxisAlignment.start,
                                       children: [
                                         DropdownButton<String>(
                                           isExpanded: true,
                                           value: countryController.text,
                                           items: countries.map((String value) {
                                             return DropdownMenuItem<String>(
                                               value: value,
                                               child: new Text(value),
                                             );
                                           }).toList(),
                                           onChanged: (country) {
                                             setState(() {
                                               countryController.text = country;
                                             });
                                           },
                                         ),
                                         SizedBox(
                                           height: size.height * .05,
                                         ),
                                       ],
                                     ),
                                   ),
                                 ),
                                 ExpandedItem(
                                   title: AppUtils.translate(context, 'city'),
                                   child: Padding(
                                     padding: EdgeInsets.symmetric(horizontal: 20),
                                     child: Column(
                                       crossAxisAlignment: CrossAxisAlignment.start,
                                       children: [
                                         MyTextFormField(
                                           hintText: AppUtils.translate(context, 'city'),
                                           hintStyle: TextStyle(fontSize: 12, color: greyColor),
                                           controller: cityController,
                                         ),
                                         SizedBox(
                                           height: size.height * .05,
                                         ),
                                       ],
                                     ),
                                   ),
                                 ),
                                 Divider(
                                   thickness: 1,
                                 ),
                                 SizedBox(
                                   height: size.height * .1,
                                 ),
                                 Align(
                                   alignment: Alignment.bottomCenter,
                                   child: updating ? Loader(size: 30) : myButton(
                                     AppUtils.translate(context, 'save'),
                                     context,
                                     onTap: () async {

                                       setState(() {
                                         updating = true;
                                       });

                                       var result = await generalBloc.updateUserInfo(UpdateUserInfoRequest(
                                         email: emailController.text,
                                         mobile: snapshot.data.mobile,
                                         country: countryController.text,
                                         address1: addressController.text,
                                         address2: snapshot.data.address2,
                                         address3: snapshot.data.address3,
                                         city: cityController.text,
                                         fname: firstNameController.text,
                                         lname: lastNameController.text,
                                       ));

                                       setState(() {
                                         updating = false;
                                       });

                                       AppUtils.showToast(msg: result.statusMessage, bgColor: Colors.blue);
                                       if(result.statusCode >= 200 && result.statusCode < 300) {
                                          AppUtils.userData.userBean.country = countryController.text;
                                          AppUtils.userData.userBean.fname = firstNameController.text;
                                          AppUtils.userData.userBean.address1 = addressController.text;
                                          AppUtils.userData.userBean.lname = lastNameController.text;
                                          AppUtils.userData.userBean.city = cityController.text;
                                          AppUtils.userData.userBean.email = emailController.text;

                                          AppUtils.saveUserData(AppUtils.userData);
                                       }
                                       },
                                   ),
                                 ),
                                 SizedBox(
                                   height: size.height * .05,
                                 ),
                               ],
                             );
                           } else if(snapshot.hasError) {
                             return Text(snapshot.error.toString());
                           } else {
                             return Loader(size: 50);
                           }
                         },
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
