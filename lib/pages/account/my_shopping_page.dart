import 'package:flutter/material.dart';
import 'package:point_zero/Bles/Bloc/AuthBloc.dart';
import 'package:point_zero/Bles/Model/Responses/sjopping_list_response.dart';
import 'package:point_zero/utils/app_utils.dart';
import 'package:point_zero/utils/consts.dart';
import 'package:point_zero/widgets/my_app_bar.dart';
import 'package:point_zero/widgets/my_loader.dart';

class MyShoppingPage extends StatefulWidget {
  @override
  _MyShoppingPageState createState() => _MyShoppingPageState();
}

class _MyShoppingPageState extends State<MyShoppingPage> {

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: [
          Container(
            width: size.width,
            height: size.height,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: Image.asset(
                  'assets/images/fav_bg.png',
                  width: size.width,
                  height: size.height,
                ).image,
                fit: BoxFit.cover,
              ),
            ),
          ),
          Container(
            width: size.width,
            height: size.height,
            color: mainColor.withOpacity(.35),
          ),
          Padding(
            padding: EdgeInsets.only(
              left: 14,
              right: 14,
              top: MediaQuery.of(context).padding.top + 10,
            ),
            child: Container(
              width: size.width,
              height: size.height,
              decoration: BoxDecoration(
                color: Colors.white,
                gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [
                    Color(0xffFAFAF9),
                    Color(0xffFDFCF9),
                    Color(0xffF9EFEA),
                  ],
                ),
                border: Border.all(color: mainColor),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                ),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                ),
                child: SingleChildScrollView(
                  physics: bouncingScrollPhysics,
                  child: Column(
                    children: [
                      MyAppBar(title: AppUtils.translate(context, 'shopping')),
                      FutureBuilder<ShoppingListResponse>(
                        future: generalBloc.getUserOrders(),
                        builder: (context, snapshot) {
                          if(snapshot.hasData) {
                            return ListView.separated(
                              itemCount: snapshot.data.data.length,
                              physics: NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              itemBuilder: (context, index) {
                                return Container(
                                  width: size.width,
                                  height: 200,
                                  padding: EdgeInsets.all(5),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        flex: 3,
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                          children: [
                                            Text('${AppUtils.translate(context, 'order_number')}: '),
                                            Text('${AppUtils.translate(context, 'phone_number')}: '),
                                            Text('${AppUtils.translate(context, 'date')}: '),
                                            Text('${AppUtils.translate(context, 'price')}: '),
                                            Text('${AppUtils.translate(context, 'delivery_price')}: '),
                                            Text('${AppUtils.translate(context, 'address')}: '),
                                            Text('${AppUtils.translate(context, 'payment_method')}: '),
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        flex: 3,
                                        child: Column(
                                          mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text('#${snapshot.data.data[index].id}'),
                                            Text('${snapshot.data.data[index].mobile}'),
                                            Text('${getDate(snapshot.data.data[index].date_added)}'),
                                            Text(
                                              '₪${snapshot.data.data[index].total_price ?? ''}',
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 1,
                                            ),
                                            Text(
                                              '₪${snapshot.data.data[index].delivery_price ?? ''}',
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 1,
                                            ),
                                            Text(
                                              '${snapshot.data.data[index].address ?? ''}',
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 1,
                                            ),
                                            Text(
                                              '${snapshot.data.data[index].payment_method ?? ''}',
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 1,
                                            ),
                                          ],
                                        ),
                                      ),
                                      // SizedBox(
                                      //   width: 12,
                                      // ),
                                      // Expanded(
                                      //   flex: 3,
                                      //   child: Container(
                                      //     child: Column(
                                      //       children: [
                                      //         Expanded(
                                      //           child: Container(
                                      //             width: double.infinity,
                                      //             margin: EdgeInsets.symmetric(
                                      //               horizontal: 10,
                                      //             ),
                                      //             decoration: BoxDecoration(
                                      //               borderRadius:
                                      //               BorderRadius.circular(30),
                                      //               border: Border.all(
                                      //                 color: thirdColor,
                                      //                 width: 1,
                                      //               ),
                                      //               image: DecorationImage(
                                      //                 fit: BoxFit.fill,
                                      //                 image: Image.network(
                                      //                   snapshot.data.data[index].cart,
                                      //                 ).image,
                                      //               ),
                                      //             ),
                                      //             child: Stack(
                                      //               clipBehavior: Clip.none,
                                      //               // children: [
                                      //               //   Positioned(
                                      //               //     right: -10,
                                      //               //     bottom: 55,
                                      //               //     child: Container(
                                      //               //       height: 25,
                                      //               //       width: 25,
                                      //               //       decoration: BoxDecoration(
                                      //               //         color: Colors.green,
                                      //               //         shape: BoxShape.circle,
                                      //               //       ),
                                      //               //       child: Icon(
                                      //               //         Icons.check,
                                      //               //         size: 16,
                                      //               //         color: Colors.white,
                                      //               //       ),
                                      //               //     ),
                                      //               //   )
                                      //               ],
                                      //             ),
                                      //           ),
                                      //         ),
                                      //       ],
                                      //     ),
                                      //   ),
                                      // ),
                                    ],
                                  ),
                                );
                              },
                              separatorBuilder: (BuildContext context, int index) {
                                return Divider(
                                  thickness: 1,
                                  height: 20,
                                );
                              },
                            );
                          } else if(snapshot.hasError) {
                            return Center(child: Text(snapshot.error.toString()),);
                          } else {
                            return Center(child: Loader());
                          }
                        }
                      ),
                      SizedBox(height: 20,),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  String getDate(String date_added) {
    DateTime dateTime = DateTime.parse(date_added);
    return '${dateTime.year}-${dateTime.month}-${dateTime.day}';
  }
}
