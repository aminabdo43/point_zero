import 'package:flutter/material.dart';
import 'package:group_button/group_button.dart';
import 'package:point_zero/utils/app_utils.dart';
import 'package:point_zero/utils/consts.dart';
import 'package:point_zero/widgets/my_app_bar.dart';
import 'package:point_zero/widgets/my_button.dart';

class NotificationsPage extends StatefulWidget {
  @override
  _NotificationsPageState createState() => _NotificationsPageState();
}

class _NotificationsPageState extends State<NotificationsPage> {
  bool enableNotifications = true;

  int selectedChannel = 0;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(children: [
        Container(
          width: size.width,
          height: size.height,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: Image.asset(
                'assets/images/fav_bg.png',
                width: size.width,
                height: size.height,
              ).image,
              fit: BoxFit.cover,
            ),
          ),
        ),
        Container(
          width: size.width,
          height: size.height,
          color: mainColor.withOpacity(.35),
        ),
        Padding(
          padding: EdgeInsets.only(
            left: 14,
            right: 14,
            top: MediaQuery.of(context).padding.top + 10,
          ),
          child: Container(
            width: size.width,
            decoration: BoxDecoration(
              color: Colors.white,
              gradient: LinearGradient(
                begin: Alignment.bottomCenter,
                end: Alignment.topCenter,
                colors: [
                  Color(0xffFAFAF9),
                  Color(0xffFDFCF9),
                  Color(0xffF9EFEA),
                ],
              ),
              border: Border.all(color: mainColor),
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30),
                topRight: Radius.circular(30),
              ),
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30),
                topRight: Radius.circular(30),
              ),
              child: Column(
                children: [
                  MyAppBar(
                    title: AppUtils.translate(context, 'notifications'),
                  ),
                  SwitchListTile(
                    title: Text(
                      AppUtils.translate(context, 'phone_notifications'),
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    value: enableNotifications,
                    onChanged: (bool val) {
                      setState(() {
                        enableNotifications = val;
                      });
                    },
                  ),
                  Divider(
                    thickness: 1,
                  ),
                  enableNotifications
                      ? Column(
                          children: [
                            SizedBox(
                              height: 10,
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                AppUtils.translate(context, 'notifications_msg1'),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            GroupButton(
                              isRadio: false,
                              spacing: 10,
                              selectedColor: mainColor,
                              onSelected: (index, isSelected) =>
                                  print('$index button is selected'),
                              buttons: [
                                AppUtils.translate(context, 'women'),
                                AppUtils.translate(context, 'children'),
                                AppUtils.translate(context, 'shoes'),
                                AppUtils.translate(context, 'men'),
                                AppUtils.translate(context, 'bags'),
                                AppUtils.translate(context, 'accessories'),
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                AppUtils.translate(context, 'notifications_msg2'),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            RadioListTile(
                              value: 0,
                              groupValue: selectedChannel,
                              onChanged: onSelectedChannel,
                              title: Text(AppUtils.translate(context, 'email'),),
                            ),
                            RadioListTile(
                              value: 1,
                              groupValue: selectedChannel,
                              onChanged: onSelectedChannel,
                              title: Text(AppUtils.translate(context, 'sms'),),
                            ),
                          ],
                        )
                      : SizedBox.shrink(),
                  SizedBox(
                    height: 10,
                  ),
                  myButton(AppUtils.translate(context, 'save'), context, onTap: () {}),
                  SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
          ),
        ),
      ]),
    );
  }

  void onSelectedChannel(int value) {
    setState(() {
      selectedChannel = value;
    });
  }
}
