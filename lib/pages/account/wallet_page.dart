import 'package:flutter/material.dart';
import 'package:point_zero/utils/app_utils.dart';
import 'package:point_zero/utils/consts.dart';
import 'package:point_zero/widgets/my_app_bar.dart';
import 'package:point_zero/widgets/my_button.dart';
import 'package:point_zero/widgets/my_text_form_field.dart';

class WalletPage extends StatefulWidget {
  @override
  _WalletPageState createState() => _WalletPageState();
}

class _WalletPageState extends State<WalletPage> {
  int paymentType = 1;

  FocusNode nameNode = FocusNode();
  FocusNode cardNumberNode = FocusNode();
  FocusNode endDateNode = FocusNode();
  FocusNode cvvNode = FocusNode();

  bool saveCreditCardData = false;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: [
          Container(
            width: size.width,
            height: size.height,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: Image.asset(
                  'assets/images/fav_bg.png',
                  width: size.width,
                  height: size.height,
                ).image,
                fit: BoxFit.cover,
              ),
            ),
          ),
          Container(
            width: size.width,
            height: size.height,
            color: mainColor.withOpacity(.35),
          ),
          Padding(
            padding: EdgeInsets.only(
              left: 14,
              right: 14,
              top: MediaQuery.of(context).padding.top + 10,
            ),
            child: Container(
              width: size.width,
              height: double.infinity,
              decoration: BoxDecoration(
                color: Colors.white,
                gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [
                    Color(0xffFAFAF9),
                    Color(0xffFDFCF9),
                    Color(0xffF9EFEA),
                  ],
                ),
                border: Border.all(color: mainColor),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                ),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                ),
                child: SingleChildScrollView(
                  physics: bouncingScrollPhysics,
                  child: Column(
                    children: [
                      MyAppBar(title: AppUtils.translate(context, 'wallet')),
                      SizedBox(
                        height: 25,
                      ),
                      Padding(
                        padding: EdgeInsets.all(12.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: List.generate(
                                2,
                                (index) => GestureDetector(
                                  onTap: () {
                                    paymentType = index;
                                    setState(() {});
                                  },
                                  child: Column(
                                    children: [
                                      AnimatedContainer(
                                        width: 120,
                                        height: 50,
                                        padding: EdgeInsets.all(8),
                                        duration: Duration(milliseconds: 300),
                                        decoration: BoxDecoration(
                                          color: index == paymentType
                                              ? mainColor
                                              : Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(30),
                                        ),
                                        child: Icon(
                                          index == 0
                                              ? Icons.money
                                              : Icons.credit_card,
                                          color: index == paymentType
                                              ? Colors.white
                                              : Colors.black,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 6,
                                      ),
                                      Text(
                                        index == 0
                                            ? AppUtils.translate(
                                                context, 'cash')
                                            : AppUtils.translate(
                                                context, 'credit_card'),
                                        textAlign: TextAlign.center,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 40,
                            ),
                            Text(AppUtils.translate(context, 'name_on_card')),
                            MyTextFormField(
                              focusNode: nameNode,
                            ),
                            SizedBox(
                              height: 40,
                            ),
                            Text(
                              AppUtils.translate(context, 'card_number'),
                            ),
                            MyTextFormField(
                              focusNode: cardNumberNode,
                            ),
                            SizedBox(
                              height: 40,
                            ),
                            Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        AppUtils.translate(
                                            context, 'expire_date'),
                                      ),
                                      MyTextFormField(
                                        focusNode: endDateNode,
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  width: 12,
                                ),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        AppUtils.translate(context, 'cvv'),
                                      ),
                                      MyTextFormField(
                                        focusNode: cvvNode,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 40,
                            ),
                            ListTile(
                              onTap: () {
                                setState(() {
                                  saveCreditCardData = !saveCreditCardData;
                                });
                              },
                              leading: Container(
                                width: 30,
                                height: 30,
                                decoration: BoxDecoration(
                                  color: mainColor,
                                  shape: BoxShape.circle,
                                ),
                                child: saveCreditCardData
                                    ? Icon(
                                        Icons.check,
                                        color: Colors.white,
                                        size: 18,
                                      )
                                    : SizedBox.shrink(),
                              ),
                              title: Text(
                                AppUtils.translate(context, 'save_credit_data'),
                              ),
                            ),
                            SizedBox(
                              height: 40,
                            ),
                            Center(
                              child: myButton(
                                AppUtils.translate(context, 'save'),
                                context,
                                onTap: () {},
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
