import 'package:flutter/material.dart';
import 'package:point_zero/pages/auth/login_page.dart';
import 'package:point_zero/pages/home/home_page.dart';
import 'package:point_zero/utils/app_utils.dart';
import 'package:point_zero/utils/consts.dart';
import 'package:point_zero/widgets/my_button.dart';

class FinishSignupPage extends StatelessWidget {
  final String from;

  const FinishSignupPage({Key key, this.from}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        width: size.width,
        height: size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: Image.asset('assets/images/signup_bg.png').image,
            fit: BoxFit.fill,
          ),
        ),
        child: Container(
          width: size.width,
          height: size.height,
          child: Stack(
            children: [
              Container(
                color: Colors.black54,
                width: size.width,
                height: size.height,
              ),
              Container(
                width: size.width,
                child: Padding(
                  padding: EdgeInsets.only(left: 20, top: MediaQuery.of(context).padding.top + 10, right: 20),
                  child: SingleChildScrollView(
                    physics: bouncingScrollPhysics,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: size.width * .25,
                        ),
                        Container(
                          width: 100,
                          height: 100,
                          decoration: BoxDecoration(
                            color: mainColor,
                            shape: BoxShape.circle,
                            boxShadow: [
                              BoxShadow(
                                color: mainColor.withOpacity(.5),
                                spreadRadius: 15,
                                blurRadius: 15,
                              ),
                            ],
                          ),
                          child: Center(
                            child: Icon(
                              Icons.check,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: size.width * .2,
                        ),
                        Text(
                          AppUtils.translate(context, 'congratulations'),
                          style: TextStyle(color: mainColor, fontSize: 25),
                        ),
                        SizedBox(
                          height: size.width * .05,
                        ),
                        Text(
                          AppUtils.translate(context, 'finish_msg'),
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white, fontSize: 15),
                        ),
                        SizedBox(
                          height: size.width * .15,
                        ),
                        SizedBox(
                          height: size.width * .33,
                        ),
                        myButton(
                          AppUtils.translate(context, 'shop_now'),
                          context,
                          onTap: () {
                            Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                builder: (_) =>
                                    from != null ? LoginPage() : HomePage(),
                              ),
                              (_) => false,
                            );
                          },
                        ),
                        SizedBox(
                          height: size.width * .05,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
