import 'package:flutter/material.dart';
import 'package:point_zero/pages/auth/verify_page.dart';
import 'package:point_zero/utils/app_utils.dart';
import 'package:point_zero/utils/consts.dart';
import 'package:point_zero/widgets/my_button.dart';
import 'package:point_zero/widgets/my_text_form_field.dart';

class ForgetPasswordPage extends StatefulWidget {
  @override
  _ForgetPasswordPageState createState() => _ForgetPasswordPageState();
}

class _ForgetPasswordPageState extends State<ForgetPasswordPage> {
  FocusNode emailNode = FocusNode();
  TextEditingController emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        width: size.width,
        height: size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: Image.asset('assets/images/login_bg.png').image,
            fit: BoxFit.fill,
          ),
        ),
        child: Container(
          width: size.width,
          height: size.height,
          child: Stack(
            children: [
              Container(
                color: Colors.black54,
                width: size.width,
                height: size.height,
              ),
              Container(
                width: size.width,
                height: size.height * .7,
                decoration: BoxDecoration(
                  color: Colors.white60,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(80),
                    bottomRight: Radius.circular(80),
                  ),
                ),
              ),
              Container(
                width: size.width,
                child: Padding(
                  padding: EdgeInsets.only(
                      left: 20,
                      top: MediaQuery.of(context).padding.top + 10,
                      right: 20,
                  ),
                  child: SingleChildScrollView(
                    physics: bouncingScrollPhysics,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment:
                              Localizations.localeOf(context).languageCode ==
                                      'ar'
                                  ? MainAxisAlignment.end
                                  : MainAxisAlignment.start,
                          children: [
                            GestureDetector(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              child: Icon(
                                Localizations.localeOf(context).languageCode ==
                                        'en'
                                    ? Icons.arrow_back_ios
                                    : Icons.arrow_forward_ios,
                                color: mainColor,
                                size: 19,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: size.width * .2,
                        ),
                        Text(
                          AppUtils.translate(context, 're_reset_password'),
                          style: TextStyle(color: Colors.black, fontSize: 30),
                        ),
                        Text(
                          AppUtils.translate(context, 'forget_password_msg'),
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.black, fontSize: 14),
                        ),
                        SizedBox(
                          height: size.width * .2,
                        ),
                        MyTextFormField(
                          hintText: AppUtils.translate(context, 'email'),
                          focusNode: emailNode,
                          controller: emailController,
                        ),
                        SizedBox(
                          height: size.width * .4,
                        ),
                        myButton(
                          AppUtils.translate(context, 'send'),
                          context,
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (_) => VerifyPage(
                                  from: 'forget_password_page',
                                ),
                              ),
                            );
                          },
                        ),
                        SizedBox(
                          height: size.width * .05,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
