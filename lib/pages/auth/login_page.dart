import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:point_zero/Bles/Bloc/AuthBloc.dart';
import 'package:point_zero/Bles/Model/Requests/LoginRequest.dart';
import 'package:point_zero/Bles/Model/Responses/LoginResponse.dart';
import 'package:point_zero/pages/auth/forget_password_page.dart';
import 'package:point_zero/pages/auth/signup_page.dart';
import 'package:point_zero/pages/home/home_contents.dart';
import 'package:point_zero/pages/home/home_page.dart';
import 'package:point_zero/providers/page_provider.dart';
import 'package:point_zero/utils/app_utils.dart';
import 'package:point_zero/utils/consts.dart';
import 'package:point_zero/widgets/clickable_text.dart';
import 'package:point_zero/widgets/my_button.dart';
import 'package:point_zero/widgets/my_button2.dart';
import 'package:point_zero/widgets/my_loader.dart';
import 'package:point_zero/widgets/my_text_form_field.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  FocusNode nameNode = FocusNode();
  FocusNode passwordNode = FocusNode();

  TextEditingController userNameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return LoadingOverlay(
      isLoading: isLoading,
      color: Colors.white,
      opacity: .5,
      progressIndicator: SpinKitCircle(
        color: mainColor,
      ),
      child: Scaffold(
        body: Container(
          width: size.width,
          height: size.height,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: Image.asset('assets/images/login_bg.png').image,
              fit: BoxFit.fill,
            ),
          ),
          child: Container(
            width: size.width,
            height: size.height,
            child: Stack(
              children: [
                Container(
                  color: Colors.black54,
                  width: size.width,
                  height: size.height,
                ),
                Container(
                  width: size.width,
                  height: size.height * .73,
                  decoration: BoxDecoration(
                    color: Colors.white60,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(80),
                      bottomRight: Radius.circular(80),
                    ),
                  ),
                ),
                Container(
                  width: size.width,
                  child: Padding(
                    padding: EdgeInsets.only(left: 20, top: MediaQuery.of(context).padding.top + 15, right: 20),
                    child: SingleChildScrollView(
                      physics: bouncingScrollPhysics,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              ClickableText(
                                text: AppUtils.translate(context, 'start_shopping_now'),
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (_) => HomePage(),
                                    ),
                                  );
                                },
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Icon(
                                Icons.arrow_forward_ios,
                                color: mainColor,
                                size: 16,
                              ),
                            ],
                          ),
                          SizedBox(
                            height: size.width * .1,
                          ),
                          Text(
                            AppUtils.translate(context, 'welcome'),
                            style: TextStyle(color: Colors.white, fontSize: 35),
                          ),
                          Text(
                            AppUtils.translate(context, 'shopping_login_msg'),
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.black, fontSize: 16),
                          ),
                          SizedBox(
                            height: size.width * .17,
                          ),
                          MyTextFormField(
                            controller: userNameController,
                            hintText: AppUtils.translate(context, 'email'),
                            focusNode: nameNode,
                          ),
                          SizedBox(
                            height: size.width * .05,
                          ),
                          MyTextFormField(
                            controller: passwordController,
                            hintText: AppUtils.translate(context, 'password'),
                            focusNode: passwordNode,
                          ),
                          SizedBox(
                            height: 18,
                          ),
                          Align(
                            child: ClickableText(
                              text: AppUtils.translate(context, 'forget_password'),
                              color: Colors.black,
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (_) => ForgetPasswordPage(),
                                  ),
                                );
                              },
                            ),
                            alignment: Localizations.localeOf(context).languageCode == 'ar' ? Alignment.centerLeft : Alignment.centerRight,
                          ),
                          SizedBox(
                            height: size.width * .34,
                          ),
                          myButton(
                            AppUtils.translate(context, 'login'),
                            context,
                            onTap: () async {
                              login();
                            },
                          ),
                          SizedBox(
                            height: size.width * .1,
                          ),
                          // Text(
                          //   AppUtils.translate(
                          //       context, 'or_through_other_accounts'),
                          //   style: TextStyle(color: Colors.white),
                          // ),
                          // SizedBox(
                          //   height: size.width * .05,
                          // ),
                          // socialButton(
                          //   Row(
                          //     mainAxisAlignment: MainAxisAlignment.center,
                          //     children: [
                          //       Text(
                          //         AppUtils.translate(
                          //             context, 'login_with_facebook'),
                          //         style: TextStyle(color: Colors.white),
                          //       ),
                          //       SizedBox(
                          //         width: 30,
                          //       ),
                          //       Image.asset('assets/images/facebook.png'),
                          //     ],
                          //   ),
                          //   () {},
                          // ),
                          // SizedBox(
                          //   height: size.width * .05,
                          // ),
                          // socialButton(
                          //   Row(
                          //     mainAxisAlignment: MainAxisAlignment.center,
                          //     children: [
                          //       Text(
                          //         AppUtils.translate(
                          //             context, 'login_with_google'),
                          //         style: TextStyle(color: Colors.white),
                          //       ),
                          //       SizedBox(
                          //         width: 30,
                          //       ),
                          //       Image.asset('assets/images/google.png'),
                          //     ],
                          //   ),
                          //   () {},
                          // ),
                          // SizedBox(
                          //   height: size.width * .05,
                          // ),
                          GestureDetector(
                            onTap: () {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (_) => SignupPage(),
                                ),
                              );
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  AppUtils.translate(context, 'have_no_account'),
                                  style: TextStyle(color: Colors.white),
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Text(
                                  AppUtils.translate(context, 'register_new'),
                                  style: TextStyle(color: mainColor),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 18,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget socialButton(Widget child, Function onTap) {
    return myButton2(
      child,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        border: Border.all(color: Colors.white, width: .5),
      ),
      onTap: onTap,
    );
  }

  void login() async {
    if(userNameController.text.isEmpty) {
      AppUtils.showToast(msg: AppUtils.translate(context, 'email_is_required'));
    } else if (passwordController.text.isEmpty){
      AppUtils.showToast(msg: AppUtils.translate(context, 'password_is_required'));
    } else {
      if (!await AppUtils.hasInternetConnection()){
        setState(() {
          isLoading = false;
        });
        AppUtils.showToast(msg: AppUtils.translate(context, 'no_internet_connection'));
        return;
      }
      setState(() {
        isLoading = true;
      });

      try {
        LoginResponse login = await generalBloc.login(LoginRequest(
            email: userNameController.text.replaceAll(' ', '').trim(),
            password: passwordController.text.replaceAll(' ', '').trim()
        ));

        if(login.user != null){
          Provider.of<PageProvider>(context, listen: false).setPage(0, HomeContents());
          setState(() {
            isLoading = false;
          });

          AppUtils.userData = UserData(
            userBean: login.user,
            token: login.token,
          );

          await AppUtils.saveUserData(AppUtils.userData);

          Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (_) => HomePage(),), (_) => false,);
        } else {
          setState(() {
            isLoading = false;
          });
          AppUtils.showToast(msg: login.msg);
        }
      } catch (error) {
        AppUtils.showToast(msg: AppUtils.translate(context, 'something_went_wrong'));
        setState(() {
          isLoading = false;
        });
      }
    }
  }
}
