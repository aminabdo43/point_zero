import 'package:flutter/material.dart';
import 'package:point_zero/pages/auth/finish_signup_page.dart';
import 'package:point_zero/utils/app_utils.dart';
import 'package:point_zero/utils/consts.dart';
import 'package:point_zero/widgets/my_button.dart';
import 'package:point_zero/widgets/my_text_form_field.dart';

class ResetPasswordPage extends StatefulWidget {
  @override
  _ResetPasswordPageState createState() => _ResetPasswordPageState();
}

class _ResetPasswordPageState extends State<ResetPasswordPage> {
  FocusNode confirmPasswordNode = FocusNode();
  FocusNode passwordNode = FocusNode();

  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();

  bool hidePassword = true;
  bool hideConfirmPassword = true;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        width: size.width,
        height: size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: Image.asset('assets/images/login_bg.png').image,
            fit: BoxFit.fill,
          ),
        ),
        child: Container(
          width: size.width,
          height: size.height,
          child: Stack(
            children: [
              Container(
                color: Colors.black54,
                width: size.width,
                height: size.height,
              ),
              Container(
                width: size.width,
                height: size.height * .75,
                decoration: BoxDecoration(
                  color: Colors.white60,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(80),
                    bottomRight: Radius.circular(80),
                  ),
                ),
              ),
              Container(
                width: size.width,
                child: Padding(
                  padding: EdgeInsets.only(left: 20, top: MediaQuery.of(context).padding.top + 10, right: 20),
                  child: SingleChildScrollView(
                    physics: bouncingScrollPhysics,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment:
                              Localizations.localeOf(context).languageCode ==
                                      'ar'
                                  ? MainAxisAlignment.end
                                  : MainAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: 5,
                            ),
                            GestureDetector(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              child: Icon(
                                Localizations.localeOf(context).languageCode ==
                                        'en'
                                    ? Icons.arrow_back_ios
                                    : Icons.arrow_forward_ios,
                                color: mainColor,
                                size: 19,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: size.width * .2,
                        ),
                        Text(
                          AppUtils.translate(context, 'create_new_password'),
                          style: TextStyle(color: Colors.white, fontSize: 30),
                        ),
                        Text(
                          AppUtils.translate(context, 'rest_password_msg'),
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.black, fontSize: 14),
                        ),
                        SizedBox(
                          height: size.width * .1,
                        ),
                        MyTextFormField(
                          hintText: AppUtils.translate(context, 'password'),
                          focusNode: passwordNode,
                          controller: passwordController,
                          obscureText: hidePassword,
                          suffixIcon: GestureDetector(
                            child: Icon(
                              hidePassword
                                  ? Icons.visibility_off
                                  : Icons.visibility,
                            ),
                            onTap: () {
                              setState(() {
                                hidePassword = !hidePassword;
                              });
                            },
                          ),
                        ),
                        SizedBox(
                          height: size.width * .03,
                        ),
                        MyTextFormField(
                          hintText:AppUtils.translate(context, 'confirm_password'),
                          focusNode: confirmPasswordNode,
                          controller: confirmPasswordController,
                          obscureText: hideConfirmPassword,
                          suffixIcon: GestureDetector(
                            child: Icon(
                              hideConfirmPassword
                                  ? Icons.visibility_off
                                  : Icons.visibility,
                            ),
                            onTap: () {
                              setState(() {
                                hideConfirmPassword = !hideConfirmPassword;
                              });
                            },
                          ),
                        ),
                        SizedBox(
                          height: size.width * .422,
                        ),
                        myButton(
                          AppUtils.translate(context, 'change'),
                          context,
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (_) => FinishSignupPage(
                                  from: 'reset_password_page',
                                ),
                              ),
                            );
                          },
                        ),
                        SizedBox(
                          height: size.width * .05,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
