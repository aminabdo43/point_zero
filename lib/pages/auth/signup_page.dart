
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:point_zero/Bles/Bloc/AuthBloc.dart';
import 'package:point_zero/Bles/Model/Requests/SignUpRequest.dart';
import 'package:point_zero/Bles/Model/Responses/SignUpResponse.dart';
import 'package:point_zero/pages/home/home_page.dart';
import 'package:point_zero/utils/app_patterns.dart';
import 'package:point_zero/utils/app_utils.dart';
import 'package:point_zero/utils/consts.dart';
import 'package:point_zero/widgets/clickable_text.dart';
import 'package:point_zero/widgets/my_button.dart';
import 'package:point_zero/widgets/my_loader.dart';
import 'package:point_zero/widgets/my_text_form_field.dart';

class SignupPage extends StatefulWidget {
  @override
  _SignupPageState createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
  FocusNode firstNameNode = FocusNode();
  FocusNode lastNameNode = FocusNode();
  FocusNode passwordNode = FocusNode();
  FocusNode emailNode = FocusNode();
  FocusNode addressNode = FocusNode();
  FocusNode phoneNumberNode = FocusNode();
  FocusNode confirmPasswordNode = FocusNode();

  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController phoneNumberController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();

  bool hidePassword = true;
  bool hideConfirmPassword = true;
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return LoadingOverlay(
      isLoading: isLoading,
      color: Colors.white,
      opacity: .5,
      progressIndicator: SpinKitCircle(
        color: mainColor,
      ),
      child: Scaffold(
        body: Container(
          width: size.width,
          height: size.height,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: Image.asset('assets/images/signup_bg.png').image,
              fit: BoxFit.fill,
            ),
          ),
          child: Container(
            width: size.width,
            height: size.height,
            child: Stack(
              children: [
                Container(
                  color: Colors.black54,
                  width: size.width,
                  height: size.height,
                ),
                Container(
                  width: size.width,
                  height: size.height * .75,
                  decoration: BoxDecoration(
                    color: Colors.white60,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(80),
                      bottomRight: Radius.circular(80),
                    ),
                  ),
                ),
                Container(
                  width: size.width,
                  child: Padding(
                    padding: EdgeInsets.only(left: 20, top: MediaQuery.of(context).padding.top + 10, right: 20),
                    child: SingleChildScrollView(
                      physics: bouncingScrollPhysics,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: Icon(Icons.arrow_back_ios,
                                  color: mainColor,
                                  size: 20,
                                ),
                              ),
                              ClickableText(text: AppUtils.translate(context, 'register')),
                            ],
                          ),
                          SizedBox(
                            height: size.width * .2,
                          ),
                          // GestureDetector(
                          //   onTap: () {},
                          //   child: Stack(
                          //     clipBehavior: Clip.none,
                          //     children: [
                          //       Container(
                          //         width: 115,
                          //         height: 115,
                          //         decoration: BoxDecoration(
                          //           color: thirdColor.withOpacity(.7),
                          //           shape: BoxShape.circle,
                          //         ),
                          //         child: Center(
                          //           child: Text(
                          //             AppUtils.translate(context, 'photo'),
                          //             style: TextStyle(
                          //                 color: mainColor, fontSize: 24,
                          //             ),
                          //           ),
                          //         ),
                          //       ),
                          //       Positioned(
                          //         right: 3,
                          //         child: Icon(
                          //           Icons.add,
                          //           color: Colors.white,
                          //           size: 30,
                          //         ),
                          //       ),
                          //     ],
                          //   ),
                          // ),
                          // SizedBox(
                          //   height: size.width * .02,
                          // ),
                          Row(
                            children: [
                              Expanded(
                                child: MyTextFormField(
                                  hintText: AppUtils.translate(context, 'first_name'),
                                  focusNode: firstNameNode,
                                  controller: firstNameController,
                                ),
                              ),
                              SizedBox(width: 30,),
                              Expanded(
                                child: MyTextFormField(
                                  hintText: AppUtils.translate(context, 'last_name'),
                                  focusNode: lastNameNode,
                                  controller: lastNameController,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: size.width * .03,
                          ),
                          MyTextFormField(
                            hintText: AppUtils.translate(context, 'email'),
                            focusNode: emailNode,
                            controller: emailController,
                            keyboardType: TextInputType.emailAddress,
                          ),
                          SizedBox(
                            height: size.width * .03,
                          ),
                          MyTextFormField(
                            hintText: AppUtils.translate(context, 'phone_number'),
                            focusNode: phoneNumberNode,
                            controller: phoneNumberController,
                            keyboardType: TextInputType.phone,
                          ),
                          SizedBox(
                            height: size.width * .03,
                          ),
                          MyTextFormField(
                            hintText: AppUtils.translate(context, 'address'),
                            focusNode: addressNode,
                            controller: addressController,
                            keyboardType: TextInputType.text,
                          ),
                          SizedBox(
                            height: size.width * .03,
                          ),
                          MyTextFormField(
                            hintText: AppUtils.translate(context, 'password'),
                            focusNode: passwordNode,
                            controller: passwordController,
                            obscureText: hidePassword,
                            suffixIcon: GestureDetector(
                              child: Icon(hidePassword ? Icons.visibility_off : Icons.visibility,
                              ),
                              onTap: () {
                                setState(() {
                                  hidePassword = !hidePassword;
                                });
                              },
                            ),
                          ),
                          SizedBox(
                            height: size.width * .03,
                          ),
                          MyTextFormField(
                            hintText: AppUtils.translate(context, 'confirm_password'),
                            focusNode: confirmPasswordNode,
                            controller: confirmPasswordController,
                            obscureText: hideConfirmPassword,
                            suffixIcon: GestureDetector(
                              child: Icon(hideConfirmPassword ? Icons.visibility_off : Icons.visibility,),
                              onTap: () {
                                setState(() {
                                  hideConfirmPassword = !hideConfirmPassword;
                                });
                              },
                            ),
                          ),
                          SizedBox(
                            height: size.width * .1,
                          ),
                          myButton(
                            AppUtils.translate(context, 'register'),
                            context,
                            onTap: () {
                              signupUser();
                            },
                          ),
                          SizedBox(
                            height: size.width * .05,
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  AppUtils.translate(context, 'have_account'),
                                  style: TextStyle(color: Colors.white),
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Text(
                                  AppUtils.translate(context, 'login'),
                                  style: TextStyle(color: mainColor),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: size.width * .05,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void signupUser() async {
    if(firstNameController.text.isEmpty) {
      AppUtils.showToast(msg: 'first_name_required');
    } else if(lastNameController.text.isEmpty) {
      AppUtils.showToast(msg: 'last_name_required');
    } else if(emailController.text.isEmpty) {
      AppUtils.showToast(msg: 'email_required');
    } else if(phoneNumberController.text.isEmpty) {
      AppUtils.showToast(msg: 'phone_number_required');
    } else if(!PatternUtils.emailIsValid(email: emailController.text)) {
      AppUtils.showToast(msg: 'enter_valid_email_address');
    } else if(addressController.text.isEmpty) {
      AppUtils.showToast(msg: 'address_required');
    } else if(passwordController.text.isEmpty) {
      AppUtils.showToast(msg: 'password_required');
    } else if(confirmPasswordController.text.isEmpty || confirmPasswordController.text != passwordController.text) {
      AppUtils.showToast(msg: 'confirm_password_not_match');
    } else {
      setState(() {
        isLoading = true;
      });

      if(!await AppUtils.hasInternetConnection()) {
        AppUtils.showToast(msg: 'no_internet_connection');
        setState(() {
          isLoading = false;
        });

        return;
      }

      try {
        SignUpResponse signupResponse = await generalBloc.signup(SignUpRequest(
          email: emailController.text.trim(),
          address: addressController.text.trim(),
          fname: firstNameController.text,
          lname: lastNameController.text,
          mobile: phoneNumberController.text,
          password: passwordController.text.trim(),
        ));

        if(signupResponse.msg != null){
          setState(() {
            isLoading = false;
          });

          Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (_) => HomePage(),), (_) => false,);
        } else {
          setState(() {
            isLoading = false;
          });
          AppUtils.showToast(msg: signupResponse.msg);
        }
      } catch (error) {
        AppUtils.showToast(msg: 'something_went_wrong');
        setState(() {
          isLoading = false;
        });
      }
    }
  }
}
