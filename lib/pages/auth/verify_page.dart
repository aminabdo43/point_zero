import 'package:flutter/material.dart';
import 'package:point_zero/pages/auth/finish_signup_page.dart';
import 'package:point_zero/pages/auth/reset_password_page.dart';
import 'package:point_zero/utils/app_utils.dart';
import 'package:point_zero/utils/consts.dart';
import 'package:point_zero/widgets/clickable_text.dart';
import 'package:point_zero/widgets/my_button.dart';
import 'package:point_zero/widgets/my_pin_text_form_field.dart';

class VerifyPage extends StatefulWidget {
  final String from;

  const VerifyPage({Key key, this.from}) : super(key: key);

  @override
  _VerifyPageState createState() => _VerifyPageState();
}

class _VerifyPageState extends State<VerifyPage> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        width: size.width,
        height: size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: Image.asset('assets/images/signup_bg.png').image,
            fit: BoxFit.fill,
          ),
        ),
        child: Container(
          width: size.width,
          height: size.height,
          child: Stack(
            children: [
              Container(
                color: Colors.black54,
                width: size.width,
                height: size.height,
              ),
              Container(
                width: size.width,
                height: size.height * .8,
                decoration: BoxDecoration(
                  color: Colors.white60,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(80),
                    bottomRight: Radius.circular(80),
                  ),
                ),
              ),
              Container(
                width: size.width,
                child: Padding(
                  padding: EdgeInsets.only(
                      left: 20,
                      top: MediaQuery.of(context).padding.top + 10,
                      right: 20,
                  ),
                  child: SingleChildScrollView(
                    physics: bouncingScrollPhysics,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            GestureDetector(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              child: Icon(
                                Icons.arrow_back_ios,
                                color: mainColor,
                                size: 19,
                              ),
                            ),
                            SizedBox(
                              width: 6,
                            ),
                            ClickableText(
                              text: AppUtils.translate(context, 'verify'),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: size.width * .05,
                        ),
                        Container(
                          width: 125,
                          height: 125,
                          decoration: BoxDecoration(
                            color: thirdColor.withOpacity(.7),
                            shape: BoxShape.circle,
                          ),
                          child: Center(
                            child: Text(
                              AppUtils.translate(context, 'photo'),
                              style: TextStyle(
                                color: mainColor,
                                fontSize: 24,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: size.width * .1,
                        ),
                        Text(
                          AppUtils.translate(context, 'verify'),
                          style: TextStyle(color: mainColor, fontSize: 25),
                        ),
                        SizedBox(
                          height: size.width * .05,
                        ),
                        Text(
                          AppUtils.translate(context, 'verify_msg'),
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.black, fontSize: 15),
                        ),
                        SizedBox(
                          height: size.width * .15,
                        ),
                        MyPinCodeTextField(
                          length: 5,
                          onChanged: (String input) {},
                          backgroundColor: Colors.transparent,
                          shape: PinCodeFieldShape.box,
                          fieldHeight: 50,
                          fieldWidth: 50,
                          inactiveColor: Colors.white,
                          activeColor: mainColor,
                          onCompleted: (String input) {},
                        ),
                        SizedBox(
                          height: size.width * .28,
                        ),
                        myButton(
                          AppUtils.translate(context, 'submit'),
                          context,
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (_) => widget.from == null
                                    ? FinishSignupPage()
                                    : ResetPasswordPage(),
                              ),
                            );
                          },
                        ),
                        SizedBox(
                          height: size.width * .05,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
