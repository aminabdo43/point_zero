import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:point_zero/Bles/Model/Responses/cart_item_model.dart';
import 'package:point_zero/utils/app_utils.dart';
import 'package:point_zero/utils/consts.dart';
import 'package:point_zero/utils/pointers.dart';

class CartItemModel extends StatefulWidget {
  final CartItem model;
  final Function onItemDeleted;

  const CartItemModel({Key key, @required this.model, this.onItemDeleted}) : super(key: key);

  @override
  _CartItemModelState createState() => _CartItemModelState();
}

class _CartItemModelState extends State<CartItemModel> {
  int counter = 1;

  @override
  void initState() {
    super.initState();

    counter = widget.model.quantity ?? 1;
  }

  @override
  Widget build(BuildContext context) {
    return Slidable(
      actionPane: SlidableDrawerActionPane(),
      actionExtentRatio: 0.25,
      child: Container(
        height: 120,
        color: Colors.transparent,
        child: Row(
          children: [
            ClipRRect(
              child: Image.network(
                widget.model.image,
                height: 120,
                fit: BoxFit.fill,
              ),
              borderRadius: BorderRadius.circular(20),
            ),
            SizedBox(
              width: 8,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    widget.model?.name ?? '',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                  ),
                  Text(
                    '${AppUtils.translate(context, 'product_number')} ${widget.model.orderNumber}',
                    style: TextStyle(fontSize: 14, color: Colors.black87),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(AppUtils.translate(context, 'quantity')),
                      Text('$counter'),
                    ],
                  ),
                ],
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    '₪' + '${(double.parse(widget.model.price) * widget.model.quantity).toStringAsFixed(1)}',
                    style: TextStyle(
                      color: mainColor,
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Expanded(
                    child: Container(
                      width: 35,
                      decoration: BoxDecoration(
                        color: greyColor,
                        borderRadius: BorderRadius.circular(30),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          GestureDetector(
                            child: Icon(Icons.add, size: 20,),
                            onTap: () {
                              setState(() {
                                counter++;
                                Pointers.updateItemQuantity(widget.model, counter);
                                widget.onItemDeleted();
                              });
                            },
                          ),
                          Text(counter.toString()),
                          GestureDetector(
                            child: Icon(Icons.remove, size: 20,),
                            onTap: () {
                              if(counter > 0) {
                                setState(() {
                                  counter--;
                                });

                                Pointers.updateItemQuantity(widget.model, counter);
                                widget.onItemDeleted();
                              }
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
      actions: <Widget>[
        IconSlideAction(
          caption: AppUtils.translate(context, 'delete'),
          color: Colors.red,
          icon: Icons.delete,
          onTap: () async {
            await Pointers.removeFromCart(widget.model);
            widget.onItemDeleted();
          }
        ),
      ],
    );
  }
}
