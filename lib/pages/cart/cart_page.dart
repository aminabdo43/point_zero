import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:point_zero/Bles/Bloc/AuthBloc.dart';
import 'package:point_zero/pages/cart/cart_item.dart';
import 'package:point_zero/pages/home/home_contents.dart';
import 'package:point_zero/pages/payment/main_payment_page.dart';
import 'package:point_zero/providers/page_provider.dart';
import 'package:point_zero/utils/app_utils.dart';
import 'package:point_zero/utils/consts.dart';
import 'package:point_zero/utils/pointers.dart';
import 'package:point_zero/widgets/my_loader.dart';
import 'package:provider/provider.dart';

class CartPage extends StatefulWidget {
  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {

  String deliveryType;
  bool gotData = false;
  bool checkingCoupon = false;
  String coupon = '';

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    if(!gotData) {
      gotData = true;
      deliveryType = AppUtils.translate(context, 'normal');
    }

    return WillPopScope(
      child: Scaffold(
        body: Stack(
          children: [
            Container(
              width: size.width,
              height: size.height,
              decoration: BoxDecoration(
                gradient: RadialGradient(
                  colors: [
                    Colors.white,
                    Color(0xffE8E6E4),
                  ],
                ),
              ),
            ),
            SingleChildScrollView(
              physics: bouncingScrollPhysics,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: MediaQuery.of(context).padding.top,
                  ),
                  appBar(context, center: true, withGrid: false, withSearch: false),
                  SizedBox(
                    height: 20,
                  ),
                  Pointers.cartItems.isEmpty
                      ? Center(
                          child: Column(
                            children: [
                              SizedBox(height: size.height * .35,),
                              Text('لا توجد عناصر'),
                            ],
                          ),
                        )
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            ListView.separated(
                              shrinkWrap: true,
                              padding: EdgeInsets.symmetric(horizontal: 14),
                              physics: NeverScrollableScrollPhysics(),
                              itemBuilder: (context, index) {
                                return CartItemModel(
                                  onItemDeleted: () {
                                    setState(() {});
                                  },
                                  model: Pointers.cartItems[index],
                                );
                              },
                              itemCount: Pointers.cartItems.length,
                              separatorBuilder: (BuildContext context, int index) {
                                return Divider(
                                  height: 30,
                                  thickness: 1,
                                );
                              },
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 14),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Divider(
                                    height: 30,
                                    thickness: 1,
                                  ),
                                  Text(
                                    AppUtils.translate(context, 'total'),
                                    style: TextStyle(
                                      color: mainColor,
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        AppUtils.translate(context, 'total_price'),
                                        style: TextStyle(fontSize: 13),
                                      ),
                                      Expanded(
                                        child: DottedLine(
                                          direction: Axis.horizontal,
                                          lineLength: double.infinity,
                                          lineThickness: 1.0,
                                          dashLength: 3.0,
                                          dashColor: Colors.grey,
                                          dashRadius: 0.0,
                                          dashGapLength: 4.0,
                                          dashGapColor: Colors.transparent,
                                          dashGapRadius: 0.0,
                                        ),
                                      ),
                                      Text(
                                        '₪${Pointers.getTotalPrice() - Pointers.couponDiscountValue}',
                                        style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 30,
                                  ),
                                  Container(
                                    padding: EdgeInsets.symmetric(horizontal: 14, vertical: 4),
                                    decoration: BoxDecoration(
                                      border: Border.all(color: greyColor),
                                      borderRadius: BorderRadius.circular(30),
                                    ),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: TextField(
                                            onChanged: (String input) {
                                              setState(() {
                                                coupon = input;
                                                AppUtils.coupon = input;
                                              });
                                            },
                                            decoration: InputDecoration(
                                              border: InputBorder.none,
                                              hintText: AppUtils.translate(context, 'coupon'),
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          width: 8,
                                        ),
                                        Pointers.couponDiscountValue != 0 ? SizedBox.shrink() : checkingCoupon ? Loader(size: 20,) : GestureDetector(child: Text(AppUtils.translate(context, 'done'),), onTap: () async {
                                          setState(() {
                                            checkingCoupon = true;
                                          });

                                          var result = await generalBloc.check_coupone(coupon);
                                          AppUtils.showToast(msg: result.message, bgColor: Colors.blue);
                                          if(result.status >= 200 && result.status < 300) {
                                            setState(() {
                                              checkingCoupon = false;
                                              Pointers.couponDiscountValue = result.data[0].amount.toDouble();
                                            });
                                          } else {
                                            AppUtils.coupon = '';
                                          }
                                        },),
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    height: 30,
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 200,
                            ),
                          ],
                        ),
                ],
              ),
            ),
           Pointers.cartItems.isEmpty ? SizedBox.shrink() : Positioned(
              bottom: 40,
              child: Container(
              width: size.width,
              height: 130,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                ),
              ),
              padding: EdgeInsets.all(16),
              child: Center(
                child: GestureDetector(
                  onTap: () {
                    Provider.of<PageProvider>(context, listen: false,).setPage(0, HomeContents(),);
                  },
                  child: Text(
                    AppUtils.translate(context, 'continue_shopping'),
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
            ),
            Pointers.cartItems.isEmpty ? SizedBox.shrink() : Positioned(
              bottom: 125,
              child: Container(
                width: size.width,
                decoration: BoxDecoration(
                  color: mainColor,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30),
                  ),
                ),
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.all(16.0),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (_) => MainPaymentPage(),
                            ),
                          );
                        },
                        child: Text(
                          AppUtils.translate(context, 'payment'),
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      onWillPop: () async {
        Provider.of<PageProvider>(context, listen: false).setPage(
          0,
          HomeContents(),
        );
        return false;
      },
    );
  }
}

Widget appBar(BuildContext context, {String title, bool withSearch = true, bool withFilter = true, bool withGrid = true, bool center = false}) {
  return PreferredSize(
    child: Material(
      elevation: 6,
      borderRadius: BorderRadius.only(
        bottomLeft: Radius.circular(30),
        bottomRight: Radius.circular(30),
      ),
      child: Container(
        height: 80,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(30),
            bottomRight: Radius.circular(30),
          ),
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Color(0xffECECEC),
              Colors.white,
            ],
          ),
        ),
        child: Padding(
          padding: EdgeInsets.only(top: 20, left: 14, right: 14),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: center ?  MainAxisAlignment.center : MainAxisAlignment.spaceBetween,
            children: [
              withSearch ? Row(
                children: [
                  GestureDetector(child: Icon(Icons.search)),
                  SizedBox(
                    width: 10,
                  ),
                  Image.asset('assets/images/filter.png'),
                ],
              ) : null,
              Padding(
                padding: EdgeInsets.only(
                  left: Localizations.localeOf(context).languageCode == 'ar'
                      ? 18
                      : 0,
                  right: Localizations.localeOf(context).languageCode == 'ar'
                      ? 0
                      : 18,
                ),
                child: Text(
                  title ?? AppUtils.translate(context, 'cart'),
                  style: TextStyle(
                    color: mainColor,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
             withGrid ? GestureDetector(child: Image.asset('assets/images/grid.png')) : null,
            ].where((element) => element != null).toList(),
          ),
        ),
      ),
    ),
    preferredSize: Size.fromRadius(0),
  );
}
