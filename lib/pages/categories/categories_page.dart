import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:point_zero/Bles/Bloc/AuthBloc.dart';
import 'package:point_zero/Bles/Model/Responses/CatResponse.dart';
import 'package:point_zero/pages/cart/cart_page.dart';
import 'package:point_zero/utils/app_utils.dart';
import 'package:point_zero/utils/consts.dart';

import 'category_item.dart';

class CategoriesPage extends StatefulWidget {
  @override
  _CategoriesPageState createState() => _CategoriesPageState();
}

class _CategoriesPageState extends State<CategoriesPage> {
  List<Category> categories;

  ScrollController listController = ScrollController();

  bool shuffle = false;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      listController.animateTo(listController.position.maxScrollExtent,
          duration: Duration(milliseconds: 100), curve: Curves.bounceIn);
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    // categories = [
    //   Category(
    //       AppUtils.translate(context, 'women'),
    //       '100 ${AppUtils.translate(context, 'item')}',
    //       'assets/images/women.jfif'),
    //   Category(
    //       AppUtils.translate(context, 'men'),
    //       '100 ${AppUtils.translate(context, 'item')}',
    //       'assets/images/men.jfif'),
    //   Category(
    //       AppUtils.translate(context, 'shoes'),
    //       '100 ${AppUtils.translate(context, 'item')}',
    //       'assets/images/shoes.jpg'),
    //   Category(
    //       AppUtils.translate(context, 'accessories'),
    //       '100 ${AppUtils.translate(context, 'item')}',
    //       'assets/images/watches.jfif'),
    //   Category(
    //       AppUtils.translate(context, 'children'),
    //       '100 ${AppUtils.translate(context, 'item')}',
    //       'assets/images/children.jpg'),
    // ];

    if(!shuffle) {
      shuffle = true;
      categories?.shuffle();
    }

    return Scaffold(
      body: Stack(
        children: [
          Container(
            width: size.width,
            height: size.height,
            decoration: BoxDecoration(
              gradient: RadialGradient(
                colors: [
                  Colors.white,
                  Color(0xffE8E6E4),
                ],
              ),
            ),
          ),
          SingleChildScrollView(
            physics: bouncingScrollPhysics,
            child: FutureBuilder<CatResponse>(
              future: generalBloc.getCat(),
              builder: (context, snapshot) {
                if(snapshot.hasError) {
                  return Center(
                    child: Text(snapshot.error.toString()),
                  );
                } else if(snapshot.hasData) {
                  return Stack(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(bottom: 190, left: 10, right: 10),
                        child: ListView.builder(
                          reverse: true,
                          controller: listController,
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          padding: EdgeInsets.zero,
                          itemBuilder: (context, index) {
                            return Align(
                              heightFactor: (0.8),
                              alignment: Alignment.topCenter,
                              child: Material(
                                elevation: 8,
                                borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(30),
                                  bottomRight: Radius.circular(30),
                                ),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(30),
                                    bottomRight: Radius.circular(30),
                                  ),
                                  child: CategoryItem(
                                    catModel: snapshot.data.data[index],
                                    image: snapshot.data.data[index].image,
                                    title: snapshot.data.data[index].name,
                                    categoryId: snapshot.data.data[index].id,
                                    hasSubcategories: snapshot.data.data[index].hasSubcats == 1,
                                  ),
                                ),
                              ),
                            );
                          },
                          itemCount: snapshot.data.data.length,
                        ),
                      ),
                      Stack(
                        children: [
                          appBar(context, title: AppUtils.translate(context, 'categories'), withSearch: false, withGrid: false, center: true),
                          Positioned(
                            child: GestureDetector(
                              child: Icon(Icons.arrow_forward, color: mainColor,),
                              onTap: () {
                                Navigator.pop(context);
                              },
                            ),
                            left: 10,
                            top: 35,
                          )
                        ],
                      )
                    ],
                  );
                }
                return Center(
                  child: Column(
                    children: [
                      SizedBox(height: size.height * .35,),
                      SpinKitCircle(
                        color: mainColor,
                      ),
                    ],
                  ),
                );
              }
            ),
          ),
        ],
      ),
    );
  }
}

class Category {
  String name;
  String count;
  String image;

  Category(this.name, this.count, this.image);
}
