import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:point_zero/Bles/Bloc/AuthBloc.dart';
import 'package:point_zero/Bles/Model/Responses/CatResponse.dart';
import 'package:point_zero/Bles/Model/Responses/SubCatResponse.dart';
import 'package:point_zero/pages/categories/category_item_details_page.dart';
import 'package:point_zero/utils/consts.dart';

class CategoryItem extends StatefulWidget {
  final String image;
  final String title;
  final int categoryId;
  final bool hasSubcategories;
  final CatModel catModel;

  const CategoryItem({Key key, this.image, this.title, this.categoryId, this.hasSubcategories, @required this.catModel,})
      : super(key: key);

  @override
  _CategoryItemState createState() => _CategoryItemState();
}

class _CategoryItemState extends State<CategoryItem> {

  bool open = false;
  double normalHeight = 250;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if(widget.hasSubcategories) {
          setState(() {
            open = !open;
          });
        } else {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => CategoryItemsDetailsPage(
                name: widget.catModel.name,
                model: widget.catModel,
              ),
            ),
          );
        }
      },
      child: Material(
        elevation: 8,
        child: Container(
          width: double.infinity,
          height: open ? MediaQuery.of(context).size.height * .8 : normalHeight,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30),
            ),
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                spreadRadius: 5,
                blurRadius: 7,
                offset: Offset(20, 20),
              ),
            ],
          ),
          child: Column(
            children: [
              Stack(
                children: [
                  Image.network(
                    widget.image,
                    fit: BoxFit.fill,
                    width: double.infinity,
                    height: normalHeight,
                  ),
                  Positioned(
                    right: 20,
                    left: 20,
                    bottom: 70,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          widget.title,
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 25,
                            shadows: <Shadow>[
                              Shadow(
                                offset: Offset(1.0, 1.0),
                                blurRadius: 3.0,
                                color: Color.fromARGB(255, 0, 0, 0),
                              ),
                            ],
                          ),
                        ),
                        FloatingActionButton(
                          heroTag: '${DateTime.now()}',
                          onPressed: () {
                            if(widget.hasSubcategories) {
                              setState(() {
                                open = !open;
                              });
                            } else {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (_) => CategoryItemsDetailsPage(
                                    name: widget.catModel.name,
                                    model: widget.catModel,
                                  ),
                                ),
                              );
                            }
                          },
                          child: Icon(
                            open ? Icons.remove : Icons.add,
                            color: Colors.black,
                          ),
                          mini: true,
                          backgroundColor: Colors.white,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              open ? Expanded(
                child: FutureBuilder<SubCatResponse>(
                    future: generalBloc.getSubCats(widget.categoryId),
                    builder: (context, snapshot) {
                      if(snapshot.hasError) {
                        return Center(
                          child: Text(snapshot.error.toString()),
                        );
                      } else if(snapshot.hasData) {
                        return ListView.builder(
                          shrinkWrap: true,
                          padding: EdgeInsets.zero,
                          physics: NeverScrollableScrollPhysics(),
                          itemBuilder: (context, index) {
                            return ListTile(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (_) => CategoryItemsDetailsPage(
                                      name: snapshot.data.data[index].sname,
                                      model: widget.catModel,
                                      subCatId: snapshot.data.data[index].id,
                                      isSubCategory: true,
                                    ),
                                  ),
                                );
                                },
                              trailing: Icon(Icons.arrow_forward_ios),
                              title: Text(snapshot.data.data[index].sname),
                            );
                            },
                          itemCount: snapshot.data.data.length,
                        );
                      }
                      return Center(
                        child: SpinKitCircle(
                          size: 30,
                          color: mainColor,
                        ),);
                    }),
              ) : SizedBox.shrink(),
            ],
          ),
        ),
      ),
    );
  }
}
