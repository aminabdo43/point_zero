
import 'package:flutter/material.dart';
import 'package:point_zero/Bles/Bloc/AuthBloc.dart';
import 'package:point_zero/Bles/Model/Responses/CatResponse.dart';
import 'package:point_zero/Bles/Model/Responses/ProductsResponse.dart';
import 'package:point_zero/Bles/Model/Responses/SubCatResponse.dart';
import 'package:point_zero/pages/home/store_item_model.dart';
import 'package:point_zero/utils/consts.dart';
import 'package:point_zero/widgets/my_loader.dart';

class CategoryItemsDetailsPage extends StatefulWidget {

  final CatModel model;
  final String name;
  final bool isSubCategory;
  final int subCatId;

  const CategoryItemsDetailsPage({Key key, this.model, this.name, this.isSubCategory = false, this.subCatId}) : super(key: key);

  @override
  _CategoryItemsDetailsPageState createState() => _CategoryItemsDetailsPageState();
}

class _CategoryItemsDetailsPageState extends State<CategoryItemsDetailsPage> {

  int selectedCategory = -1;
  int selectedSubCategory = -1;

  bool gotData = false;

  Future future;

  @override
  void initState() {
    super.initState();

    if(widget.isSubCategory) {
      future = generalBloc.getProducts(widget.subCatId);
    } else {
      future = generalBloc.getProducts(widget.model.id);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.name),
        backgroundColor: mainColor,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Material(
              elevation: 8,
              child: Container(
                width: double.infinity,
                height: 280,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(30),
                    bottomRight: Radius.circular(30),
                  ),
                ),
                child: Column(
                  children: [
                    Expanded(
                      child: Image.network(
                        widget.model.image,
                        fit: BoxFit.fill,
                        width: double.infinity,
                        height: 250,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Center(
                      child: Text(
                        widget.model.name,
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 25,
                          shadows: <Shadow>[
                            Shadow(
                              offset: Offset(1.0, 1.0),
                              blurRadius: 3.0,
                              color: Color.fromARGB(255, 0, 0, 0),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            widget.isSubCategory ? SizedBox.shrink() :
            FutureBuilder<SubCatResponse>(
              future: generalBloc.getSubCats(widget.model.id),
              builder: (context, snapshot) {
                if(snapshot.hasData) {
                  if(!gotData) {
                    gotData = true;
                    if(snapshot.data.data.isNotEmpty) {
                      selectedSubCategory = snapshot.data.data[0].id;
                      future = generalBloc.getProducts(selectedSubCategory);
                    }
                  }
                  return Container(
                    height: 35,
                    width: double.infinity,
                    child: ListView.builder(
                      physics: bouncingScrollPhysics,
                      itemBuilder: (context, index) {
                        return GestureDetector(
                          onTap: () {
                            setState(() {
                              selectedCategory = index;
                              selectedSubCategory = snapshot.data.data[index].id;
                              future = generalBloc.getProducts(selectedSubCategory);
                            });
                          },
                          child: Container(
                            width: 90,
                            margin: EdgeInsets.symmetric(horizontal: 5),
                            decoration: BoxDecoration(
                              color: selectedCategory == (index) ? mainColor : Colors.transparent,
                              borderRadius: BorderRadius.circular(40),
                              border: Border.all(color: mainColor, width: 1),
                            ),
                            child: Center(
                              child: Text(
                                snapshot.data.data[(index)].sname,
                                style: TextStyle(
                                  color: selectedCategory == (index)
                                      ? Colors.white
                                      : Colors.black,
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                      itemCount: snapshot.data.data.length,
                      scrollDirection: Axis.horizontal,
                    ),
                  );
                } else {
                  return Loader(size: 30,);
                }
              }
            ),
            SizedBox(
              height: 20,
            ),
            FutureBuilder<ProductsResponse>(
              future: future,
              builder: (context, snapshot) {
                if(snapshot.hasData) {
                  return Container(
                    height: 300,
                    child: ListView.builder(
                      physics: bouncingScrollPhysics,
                      addAutomaticKeepAlives: true,
                      itemBuilder: (context, index) {
                        return Padding(
                            padding: EdgeInsets.only(
                              right: Localizations.localeOf(context).languageCode == 'ar' ? 18 : 0,
                              left: Localizations.localeOf(context).languageCode == 'en' ? 18 : 0,
                            ),
                            child: StoreItemModel(
                              title: snapshot.data.data[index].name,
                              categoryName: widget.name,
                              price: snapshot.data.data[index].price.toString(),
                              image: snapshot.data.data[index].mainImage,
                              productId: snapshot.data.data[index].id,
                              isNew: false,
                            ),
                          );
                        },
                        itemCount: snapshot.data.data.length,
                        scrollDirection: Axis.horizontal,
                      ),
                    );
                  } else if (snapshot.hasError) {
                    return Center(
                      child: Text(snapshot.error.toString()),
                    );
                  } else {
                    return Loader(
                      size: 30,
                    );
                  }
                }),
          ],
        ),
      ),
    );
  }
}
