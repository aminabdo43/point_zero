import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_animated_dialog/flutter_animated_dialog.dart';
import 'package:point_zero/Bles/Bloc/AuthBloc.dart';
import 'package:point_zero/Bles/Model/Responses/ProducResponse.dart';
import 'package:point_zero/Bles/Model/Responses/cart_item_model.dart';
import 'package:point_zero/utils/app_utils.dart';
import 'package:point_zero/utils/consts.dart';
import 'package:point_zero/utils/pointers.dart';
import 'package:point_zero/widgets/my_button2.dart';

class Details {
  String type;
  String name;
  List<String> data;

  Details({this.type, this.name, this.data});

  Details.fromJson(Map<String, dynamic> json) {
    type = json['type'];
    name = json['name'];
    data = json['data'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type'] = this.type;
    data['name'] = this.name;
    data['data'] = this.data;
    return data;
  }
}

class ItemDetails extends StatefulWidget {

  final ProductModel productModel;

  const ItemDetails({Key key, @required this.productModel}) : super(key: key);

  @override
  _ItemDetailsState createState() => _ItemDetailsState();
}

class _ItemDetailsState extends State<ItemDetails> {

  List<Details> details = [];

  String selectedColor = '';
  String selectedSize = '';

  bool gotData = false;

  @override
  Widget build(BuildContext context) {

    if(!gotData) {
      details.clear();
      json.decode(widget.productModel.details).forEach((e) {
        details.add(Details.fromJson(e));
      });

      gotData = true;
    }

    return Wrap(
      children: [
        Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30),
              topRight: Radius.circular(30),
            ),
          ),
          padding: EdgeInsets.all(12.0),
          child: Column(
            children: [
              Text(
                AppUtils.translate(context, 'details'),
                style: TextStyle(color: mainColor, fontWeight: FontWeight.bold),
              ),
              Container(
                width: 100,
                height: 5,
                decoration: BoxDecoration(
                  color: Colors.grey,
                  borderRadius: BorderRadius.circular(30),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                '${widget.productModel.description}',
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 30,
              ),
              details.isEmpty ? null : Wrap(
                children: List.generate(details.length, (index) => DropItem(
                  label: details[index].name,
                  data: details[index].data,
                  onValueSelected: (String value) {
                    if(details[index].name == 'الحجم' || details[index].name == 'القياس') {
                      selectedSize = value;
                    }
                    if(details[index].name == 'اللون' || details[index].name == 'الالوان') {
                      selectedColor = value;
                    }
                  },
                )),
              ),
              SizedBox(
                height: 30,
              ),
              widget.productModel.discount == null || widget.productModel.discount == 0 ? null : Text(
                ' ₪' + '${widget.productModel.price}',
                style: TextStyle(
                  fontSize: 13,
                  fontWeight: FontWeight.bold,
                  color: mainColor,
                ),
              ),
              widget.productModel.discount == null || widget.productModel.discount == 0 ? null : Text(
                AppUtils.translate(context, 'off') + ' ₪' + '${widget.productModel.discount} ',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 13,
                  fontWeight: FontWeight.bold,
                  color: mainColor,
                ),
              ),
              Text(
                '₪${getPrice()}',
                style: TextStyle(
                  fontSize: 35,
                  fontWeight: FontWeight.bold,
                  color: mainColor,
                ),
              ),
              SizedBox(
                height: 25,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding: EdgeInsets.all(12),
                    decoration: BoxDecoration(
                      color: mainColor.withOpacity(.2),
                      borderRadius: BorderRadius.circular(30),
                    ),
                    child: GestureDetector(
                      onTap: () async {
                        await generalBloc.add_wishList(widget.productModel.id);
                        showAnimatedDialog(
                          context: context,
                          barrierDismissible: true,
                          builder: (BuildContext context) {
                            return ClassicGeneralDialogWidget(
                              contentText: AppUtils.translate(context, 'product_added_to_wishlist'),
                              titleText: AppUtils.translate(context, 'done'),
                              negativeText: '',
                              positiveText: '',
                            );
                          },
                          animationType: DialogTransitionType.size,
                          curve: Curves.fastOutSlowIn,
                          duration: Duration(seconds: 1),
                        );
                      },
                      child: Text(
                        AppUtils.translate(context, 'add_with_list'),
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 20, color: Colors.black),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 25,
                  ),
                  Container(
                    padding: EdgeInsets.all(12),
                    decoration: BoxDecoration(
                      color: mainColor.withOpacity(.2),
                      borderRadius: BorderRadius.circular(30),
                    ),
                    child: GestureDetector(
                      onTap: () async {
                        Pointers.addToCart(
                          CartItem(
                            orderNumber: widget.productModel.id.toString(),
                            name: widget.productModel.name,
                            price: getPrice().toString(),
                            image: widget.productModel.mainImage,
                            quantity: 1,
                            id: widget.productModel.id.toString(),
                            color: selectedColor,
                            size: selectedSize,
                          ),
                        );
                        showAnimatedDialog(
                          context: context,
                          barrierDismissible: true,
                          builder: (BuildContext context) {
                            return ClassicGeneralDialogWidget(
                              contentText: AppUtils.translate(context, 'product_added_to_cart'),
                              negativeText: '',
                              titleText: AppUtils.translate(context, 'done'),
                              positiveText: '',
                            );
                          },
                          animationType: DialogTransitionType.size,
                          curve: Curves.fastOutSlowIn,
                          duration: Duration(seconds: 1),
                        );
                      },
                      child: Text(
                        AppUtils.translate(context, 'add_to_cart'),
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 20, color: Colors.black),
                      ),
                    ),
                  ),
                ].where((element) => element != null).toList(),
              ),
              SizedBox(
                height: 25,
              ),
            ].where((element) => element != null).toList(),
          ),
        )
      ].where((element) => element != null).toList(),
    );
  }

  String getPrice() {
   if(widget.productModel.discount != null && widget.productModel.discount > 0) {
     double p = (widget.productModel.price - widget.productModel.discount).toDouble();
     String price = '$p';
     return price;
   } else {
     return widget.productModel.price.toStringAsFixed(2);
   }
  }
}

class DropItem extends StatefulWidget {
  final List<String> data;
  final String label;
  final Function(String selectedValue) onValueSelected;

  const DropItem({Key key, this.data, this.label, this.onValueSelected}) : super(key: key);

  @override
  _DropItemState createState() => _DropItemState();
}

class _DropItemState extends State<DropItem> {

  String label;
  String selected;

  @override
  void initState() {
    super.initState();

    if(widget.data.isNotEmpty) {
      selected = widget.data[0];
    }
  }

  @override
  Widget build(BuildContext context) {
    return myButton2(
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            widget.label ?? '',
            style: TextStyle(color: Colors.black),
          ),
          DropdownButton<String>(
            value: selected,
            underline: SizedBox.shrink(),
            items: widget.data.map((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
            onChanged: (color) {
              setState(() {
                selected = color;
                widget.onValueSelected(selected);
              });
            },
          ),
        ],
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(30),
        border: Border.all(color: mainColor),
      ),
    );
  }
}

