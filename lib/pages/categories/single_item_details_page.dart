import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:point_zero/Bles/Bloc/AuthBloc.dart';
import 'package:point_zero/Bles/Model/Responses/ProducResponse.dart';
import 'package:point_zero/utils/app_utils.dart';
import 'package:point_zero/utils/consts.dart';

import 'item_details.dart';

class SingleItemDetailsPage extends StatefulWidget {

  final int productId;

  const SingleItemDetailsPage({Key key, this.productId}) : super(key: key);

  @override
  _SingleItemDetailsPageState createState() => _SingleItemDetailsPageState();
}

class _SingleItemDetailsPageState extends State<SingleItemDetailsPage> {

  int currentImage = 0;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: FutureBuilder<ProducResponse>(
        future: generalBloc.getProduct(widget.productId),
        builder: (context, snapshot) {
          if(snapshot.hasData) {
            return Stack(
              children: [
                Container(
                  width: size.width,
                  height: size.height,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: Image.asset(
                        'assets/images/fav_bg.png',
                        width: size.width,
                        height: size.height,
                      ).image,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Container(
                  width: size.width,
                  height: size.height,
                  color: mainColor.withOpacity(.35),
                ),
                Padding(
                  padding: EdgeInsets.only(
                    left: 14,
                    right: 14,
                    top: MediaQuery.of(context).padding.top + 10,
                  ),
                  child: Container(
                    width: size.width,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      gradient: LinearGradient(
                        begin: Alignment.bottomCenter,
                        end: Alignment.topCenter,
                        colors: [
                          Color(0xffFAFAF9),
                          Color(0xffFDFCF9),
                          Color(0xffF9EFEA),
                        ],
                      ),
                      border: Border.all(color: mainColor),
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30),
                      ),
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30),
                      ),
                      child: Column(
                        children: [
                          Expanded(
                            child: Stack(
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(30),
                                    topRight: Radius.circular(30),
                                  ),
                                  child: PageView.builder(
                                    onPageChanged: (int page) {
                                      setState(() {
                                        currentImage = page;
                                      });
                                    },
                                    physics: bouncingScrollPhysics,
                                    scrollDirection: Axis.vertical,
                                    itemBuilder: (context, index) {
                                      return InteractiveViewer(
                                        child: Image.network(
                                          snapshot.data.data.images.split(',')[index],
                                          fit: BoxFit.fill,
                                        ),
                                      );
                                    },
                                    itemCount: snapshot.data.data.images.split(',').length,
                                  ),
                                ),
                                Positioned(
                                  left: 5,
                                  top: 150,
                                  bottom: 150,
                                  child: Container(
                                    width: 5,
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: List.generate(
                                        snapshot.data.data.images.split(',').length, (index) => AnimatedContainer(
                                          margin: EdgeInsets.symmetric(vertical: 3),
                                          width: double.infinity,
                                          height: 20,
                                          duration: Duration(milliseconds: 200),
                                          decoration: BoxDecoration(
                                            color: currentImage == index
                                                ? mainColor
                                                : Colors.white,
                                            borderRadius: BorderRadius.circular(10),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Positioned(
                  bottom: 1,
                  left: 15,
                  right: 15,
                  child: GestureDetector(
                    onTap: () {
                      showModalBottomSheet(
                        context: context,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(30),
                            topRight: Radius.circular(30),
                          ),
                        ),
                        isScrollControlled: true,
                        builder: (context) {
                          return ItemDetails(
                            productModel: snapshot.data.data,
                          );
                        },
                      );
                    },
                    child: Container(
                      height: 60,
                      padding: EdgeInsets.all(8),
                      child: Center(
                        child: Column(
                          children: [
                            Text(AppUtils.translate(context, 'details'), style: TextStyle(color: mainColor, fontWeight: FontWeight.bold),),
                            Container(
                              width: 110,
                              height: 5,
                              decoration: BoxDecoration(
                                color: Colors.grey,
                                borderRadius: BorderRadius.circular(30),
                              ),
                            ),
                          ],
                        ),
                      ),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30),
                        ),
                      ),
                    ),
                  ),
                ),
                // Positioned(
                //   bottom: 0,
                //   left: 15,
                //   right: 15,
                //   child: GestureDetector(
                //     onTap: () {
                //       AppUtils.showToast(msg: 'done');
                //       Pointers.addToCart(
                //         CartItem(
                //           orderNumber: snapshot.data.data.id.toString(),
                //           name: snapshot.data.data.name,
                //           price: snapshot.data.data.price.toString(),
                //           image: snapshot.data.data.mainImage,
                //           quantity: 1,
                //           id: snapshot.data.data.id.toString(),
                //         ),
                //       );
                //     },
                //     child: Container(
                //       height: 60,
                //       child: Center(
                //         child: Text(
                //           AppUtils.translate(context, 'add_to_cart'),
                //           style: TextStyle(
                //             color: Colors.white,
                //             fontSize: 18,
                //             fontWeight: FontWeight.bold,
                //           ),
                //         ),
                //       ),
                //       decoration: BoxDecoration(
                //         color: mainColor,
                //         borderRadius: BorderRadius.only(
                //           topLeft: Radius.circular(30),
                //           topRight: Radius.circular(30),
                //         ),
                //       ),
                //     ),
                //   ),
                // ),
              ],
            );
          } else if(snapshot.hasError) {
            return Center(child: Text(snapshot.error.toString()),);
          }
          return Center(
            child: SpinKitCircle(
              color: mainColor,
            ),
          );
        }
      ),
    );
  }
}
