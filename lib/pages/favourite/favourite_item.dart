import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:point_zero/Bles/Bloc/AuthBloc.dart';
import 'package:point_zero/Bles/Model/Responses/GetWishListResponse.dart';
import 'package:point_zero/Bles/Model/Responses/ProducResponse.dart';
import 'package:point_zero/pages/categories/single_item_details_page.dart';
import 'package:point_zero/utils/app_utils.dart';
import 'package:point_zero/widgets/my_loader.dart';

class FavouriteItem extends StatefulWidget {
  final DataBean model;
  final Function onDelete;

  const FavouriteItem({Key key, @required this.model, this.onDelete}) : super(key: key);

  @override
  _FavouriteItemState createState() => _FavouriteItemState();
}

class _FavouriteItemState extends State<FavouriteItem> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<ProducResponse>(
      future: generalBloc.getProduct(widget.model?.product),
      builder: (context, snapshot) {
        if(snapshot.hasData) {
          return Slidable(
            actionPane: SlidableDrawerActionPane(),
            actionExtentRatio: 0.25,
            child: GestureDetector(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (_) => SingleItemDetailsPage(
                  productId: snapshot.data?.data?.id,
                )));
              },
              child: Container(
                margin: EdgeInsets.symmetric(vertical: 8, horizontal: 4),
                color: Colors.transparent,
                child: Row(
                  children: [
                    Expanded(
                      child: ClipRRect(
                        child: Image.network(
                          snapshot.data?.data?.mainImage,
                          fit: BoxFit.cover,
                        ),
                        borderRadius: BorderRadius.circular(20),
                      ),
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    Expanded(
                      flex: 2,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Text(
                            snapshot.data?.data?.name,
                            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                          ),
                          Text(
                            '${AppUtils.translate(context, 'product_number')} ${snapshot.data?.data?.id ?? ''}',
                            style: TextStyle(fontSize: 13, color: Colors.black87),
                          ),
                          Text(
                            '${AppUtils.translate(context, 'quantity')} ${snapshot.data?.data?.quantity ?? ''}',
                            style: TextStyle(fontSize: 13, color: Colors.black87),
                          ),
                          Text(
                            '${snapshot.data?.data?.description ?? ''}',
                            style: TextStyle(fontSize: 13, color: Colors.black87),
                          ),
                          Text(
                            '${snapshot.data?.data?.keywords ?? ''}',
                            style: TextStyle(fontSize: 13, color: Colors.black87),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            actions: <Widget>[
              IconSlideAction(
                caption: AppUtils.translate(context, 'delete'),
                color: Colors.red,
                icon: Icons.delete,
                onTap: () async {
                  await generalBloc.delete_wish_list(snapshot.data.data.id);
                  widget.onDelete();
                },
              ),
            ],
          );
        } else {
          return Loader(size: 30,);
        }
      }
    );
  }
}
