
import 'package:flutter/material.dart';
import 'package:point_zero/Bles/Bloc/AuthBloc.dart';
import 'package:point_zero/Bles/Model/Responses/GetWishListResponse.dart';
import 'package:point_zero/pages/home/home_contents.dart';
import 'package:point_zero/providers/page_provider.dart';
import 'package:point_zero/utils/app_utils.dart';
import 'package:point_zero/utils/consts.dart';
import 'package:point_zero/widgets/my_loader.dart';
import 'package:provider/provider.dart';

import 'favourite_item.dart';

class FavouritePage extends StatefulWidget {
  @override
  _FavouritePageState createState() => _FavouritePageState();
}

class _FavouritePageState extends State<FavouritePage> {

  Future future;

  @override
  void initState() {
    super.initState();

    future = generalBloc.getWishList();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: () async {
        Provider.of<PageProvider>(context, listen: false).setPage(
          0,
          HomeContents(),
        );

        return false;
      },
      child: Scaffold(
        body: Stack(
          children: [
            Container(
              width: size.width,
              height: size.height,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: Image.asset(
                    'assets/images/fav_bg.png',
                    width: size.width,
                    height: size.height,
                  ).image,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Container(
              width: size.width,
              height: size.height,
              color: mainColor.withOpacity(.35),
            ),
            Padding(
              padding: EdgeInsets.only(
                left: 14,
                right: 14,
                top: MediaQuery.of(context).padding.top + 10,
              ),
              child: Container(
                height: size.height,
                decoration: BoxDecoration(
                  color: Colors.white,
                  gradient: LinearGradient(
                    begin: Alignment.bottomCenter,
                    end: Alignment.topCenter,
                    colors: [
                      Color(0xffFAFAF9),
                      Color(0xffFDFCF9),
                      Color(0xffF9EFEA),
                    ],
                  ),
                  border: Border.all(color: mainColor),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30),
                  ),
                ),
                child: FutureBuilder<GetWishListResponse>(
                  future: future,
                  builder: (context, snapshot) {
                    if(snapshot.hasData) {
                      return SingleChildScrollView(
                        child: Column(
                          children: [
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              AppUtils.translate(context, 'wishlist'),
                              style: TextStyle(color: mainColor, fontSize: 24),
                            ),
                            Divider(
                              color: mainColor,
                              height: 25,
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 12),
                              child: Column(
                                children: List.generate(snapshot.data.data.length, (index) =>  FavouriteItem(
                                  model: snapshot.data.data[index],
                                  onDelete: () {
                                    future = generalBloc.getWishList();
                                    setState(() {});
                                  },
                                )),
                              ),
                            ),
                            SizedBox(
                              height: 100,
                            ),
                          ],
                        ),
                      );
                    } else {
                      return Loader();
                    }
                  }
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
