import 'package:flutter/material.dart';
import 'package:group_button/group_button.dart';
import 'package:point_zero/pages/account/expand_item.dart';
import 'package:flutter_range_slider/flutter_range_slider.dart' as frs;
import 'package:point_zero/utils/app_utils.dart';
import 'package:point_zero/utils/consts.dart';
import 'package:point_zero/widgets/my_app_bar.dart';
import 'package:point_zero/widgets/my_button.dart';

class FilterPage extends StatefulWidget {
  @override
  _FilterPageState createState() => _FilterPageState();
}

class _FilterPageState extends State<FilterPage> {
  double _lowerValue = 20.0;
  double _upperValue = 80.0;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: [
          Container(
            width: size.width,
            height: size.height,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: Image.asset(
                  'assets/images/fav_bg.png',
                  width: size.width,
                  height: size.height,
                ).image,
                fit: BoxFit.cover,
              ),
            ),
          ),
          Container(
            width: size.width,
            height: size.height,
            color: mainColor.withOpacity(.35),
          ),
          Padding(
            padding: EdgeInsets.only(
              left: 14,
              right: 14,
              top: MediaQuery.of(context).padding.top + 10,
            ),
            child: Container(
              width: size.width,
              height: double.infinity,
              decoration: BoxDecoration(
                color: Colors.white,
                gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [
                    Color(0xffFAFAF9),
                    Color(0xffFDFCF9),
                    Color(0xffF9EFEA),
                  ],
                ),
                border: Border.all(color: mainColor),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                ),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                ),
                child: SingleChildScrollView(
                  physics: bouncingScrollPhysics,
                  child: Column(
                    children: [
                      MyAppBar(
                        title: AppUtils.translate(context, 'filter'),
                      ),
                      Column(
                        children: [
                          ExpandedItem(
                            title: AppUtils.translate(context, 'clothes_type'),
                            child: GroupButton(
                              isRadio: false,
                              spacing: 10,
                              selectedColor: mainColor,
                              onSelected: (index, isSelected) =>
                                  print('$index button is selected'),
                              buttons: [
                                "فساتين",
                                "بنطال جينز",
                                "ملابس رياضة",
                                "محجبات",
                                "جاكيت",
                                "جينز",
                                "فساتين",
                                "بنطال جينز",
                                "ملابس رياضة",
                                "محجبات",
                                "جاكيت",
                                "جينز",
                              ],
                            ),
                          ),
                          Divider(
                            thickness: 1,
                          ),
                          ExpandedItem(
                            title: AppUtils.translate(context, 'trade_mark'),
                            child: GroupButton(
                              isRadio: false,
                              selectedColor: mainColor,
                              spacing: 10,
                              onSelected: (index, isSelected) =>
                                  print('$index button is selected'),
                              buttons: [
                                "ZARA",
                                "CHANEL",
                                "H&M",
                                "MANGO",
                                "VERSACA",
                                "CUCCIO",
                                "DIOR",
                              ],
                            ),
                          ),
                          Divider(
                            thickness: 1,
                          ),
                          ExpandedItem(
                            title: AppUtils.translate(context, 'price'),
                            child: Column(
                              children: [
                                SizedBox(
                                  height: size.height * .05,
                                ),
                                frs.RangeSlider(
                                  min: 0.0,
                                  max: 100.0,
                                  lowerValue: _lowerValue,
                                  upperValue: _upperValue,
                                  divisions: 5,
                                  showValueIndicator: true,
                                  valueIndicatorMaxDecimals: 1,
                                  onChanged: (double newLowerValue,
                                      double newUpperValue) {
                                    setState(() {
                                      _lowerValue = newLowerValue;
                                      _upperValue = newUpperValue;
                                    });
                                  },
                                ),
                                SizedBox(
                                  height: size.height * .05,
                                ),
                              ],
                            ),
                          ),
                          Divider(
                            thickness: 1,
                          ),
                          ExpandedItem(
                            title: AppUtils.translate(context, 'size'),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  AppUtils.translate(context, 'clothes'),
                                ),
                                GroupButton(
                                  isRadio: false,
                                  spacing: 10,
                                  selectedColor: mainColor,
                                  onSelected: (index, isSelected) =>
                                      print('$index button is selected'),
                                  buttons: [
                                    "S",
                                    "M",
                                    "XL",
                                    "XXL",
                                    "XXXl",
                                  ],
                                ),
                                SizedBox(
                                  height: size.height * .05,
                                ),
                                Text(
                                  AppUtils.translate(context, 'shoes'),
                                ),
                                GroupButton(
                                  isRadio: false,
                                  selectedColor: mainColor,
                                  spacing: 10,
                                  onSelected: (index, isSelected) =>
                                      print('$index button is selected'),
                                  buttons: [
                                    "36",
                                    "37",
                                    "38",
                                    "39",
                                    "40",
                                    "41",
                                    "42",
                                    "43",
                                    "44",
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Divider(
                            thickness: 1,
                          ),
                          ExpandedItem(
                            title: AppUtils.translate(context, 'color'),
                            child: GroupButton(
                              isRadio: false,
                              spacing: 10,
                              selectedColor: mainColor,
                              onSelected: (index, isSelected) =>
                                  print('$index button is selected'),
                              buttons: [
                                "احمر",
                                "اخضر",
                                "اصفر",
                                "بيج",
                                "بني",
                                "اسود",
                                "ابيض",
                                "برتقالي",
                              ],
                            ),
                          ),
                          Divider(
                            thickness: 1,
                          ),
                          SizedBox(
                            height: size.height * .1,
                          ),
                          myButton(
                            AppUtils.translate(context, 'submit'),
                            context,
                            onTap: () {},
                          ),
                          SizedBox(
                            height: size.height * .05,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
