import 'package:flutter/material.dart';
import 'package:point_zero/pages/home/store_item_model.dart';
import 'package:point_zero/utils/consts.dart';
import 'package:point_zero/widgets/my_sliver_grid_delegate.dart';

class AllItemsPage extends StatefulWidget {
  final String title;
  final List list;
  final bool isRecommended;

  const AllItemsPage({Key key, this.title, this.list, this.isRecommended = false}) : super(key: key);

  @override
  _AllItemsPageState createState() => _AllItemsPageState();
}

class _AllItemsPageState extends State<AllItemsPage> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: [
          Container(
            width: size.width,
            height: size.height,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: Image.asset(
                  'assets/images/fav_bg.png',
                  width: size.width,
                  height: size.height,
                ).image,
                fit: BoxFit.cover,
              ),
            ),
          ),
          Container(
            width: size.width,
            height: size.height,
            color: mainColor.withOpacity(.35),
          ),
          Padding(
            padding: EdgeInsets.only(
              left: 14,
              right: 14,
              top: MediaQuery.of(context).padding.top + 10,
            ),
            child: Container(
              height: size.height,
              decoration: BoxDecoration(
                color: Colors.white,
                gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [
                    Color(0xffFAFAF9),
                    Color(0xffFDFCF9),
                    Color(0xffF9EFEA),
                  ],
                ),
                border: Border.all(color: mainColor),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                ),
              ),
              child: Column(
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    widget.title,
                    style: TextStyle(color: mainColor, fontSize: 24),
                  ),
                  Divider(
                    color: mainColor,
                    height: 20,
                  ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.only(right: 12),
                      child: GridView.builder(
                        physics: bouncingScrollPhysics,
                        itemBuilder: (context, index) {
                          return Container(
                            height: 350,
                            margin: EdgeInsets.symmetric(vertical: 5),
                            child: StoreItemModel(
                              title: widget.list[index].name,
                              categoryName: widget.list[index].keywords,
                              price: widget.list[index].price.toString(),
                              image: widget.list[index].mainImage,
                              productId: widget.list[index].id,
                              isNew: false,
                            ),
                          );
                        },
                        itemCount: widget.isRecommended ? widget.list.length : widget.list.length,
                        gridDelegate: MySliverGridDelegateWithFixedCrossAxisCountAndFixedHeight(
                          crossAxisCount: 2,
                          height: 250
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
