import 'package:flutter/material.dart';
import 'package:point_zero/Bles/Bloc/AuthBloc.dart';
import 'package:point_zero/Bles/Model/Responses/SliderResponse.dart';
import 'package:point_zero/utils/consts.dart';
import 'package:point_zero/widgets/my_loader.dart';

import 'home_slider_part.dart';
import 'new_part.dart';
import 'recommended_part.dart';

class HomeContents extends StatefulWidget {
  @override
  _HomeContentsState createState() => _HomeContentsState();
}

class _HomeContentsState extends State<HomeContents> {
  Future future;

  @override
  void initState() {
    super.initState();

    future = generalBloc.getSlider();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
        physics: bouncingScrollPhysics,
        child: Column(
          children: [
            HomeSliderPart(),
            SizedBox(
              height: 30,
            ),
            Container(
              width: size.width,
              padding: EdgeInsets.symmetric(vertical: 10),
              child: FutureBuilder<SliderResponse>(
                future: future,
                builder: (context, snapshot) {
                  if(snapshot.hasData) {
                    return ListView.builder(
                      itemCount: snapshot.data.data.length,
                      itemBuilder: (context, index) {
                        return NewPart(
                          categoryName: snapshot.data.data[index].name,
                          data: snapshot.data.data[index].products,
                        );
                      },
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                    );
                  } else {
                    return Loader(size: 50,);
                  }
                }),
            ),
            // RecommendedPart(),
            // SizedBox(
            //   height: 90,
            // ),
          ],
        ),
      ),
    );
  }
}
