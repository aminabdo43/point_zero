
import 'package:flutter/material.dart';
import 'package:point_zero/providers/page_provider.dart';
import 'package:point_zero/widgets/bottom_navigation_bar.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      bottomNavigationBar: MyBottomNavigatonBar(),
      body: Consumer<PageProvider>(
        builder: (context, provider, child) {
          return provider.page;
        },
      ),
    );
  }
}
