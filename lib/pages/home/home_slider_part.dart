import 'dart:ui';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:page_transition/page_transition.dart';
import 'package:point_zero/Bles/Bloc/AuthBloc.dart';
import 'package:point_zero/Bles/Model/Responses/CatResponse.dart';
import 'package:point_zero/pages/categories/categories_page.dart';
import 'package:point_zero/pages/categories/category_item_details_page.dart';
import 'package:point_zero/utils/consts.dart';

import 'search_page.dart';

class HomeSliderPart extends StatefulWidget {
  @override
  _HomeSliderPartState createState() => _HomeSliderPartState();
}

class _HomeSliderPartState extends State<HomeSliderPart> {
  CarouselControllerImpl pageController;
  int currentPage = 0;

  Future future;

  @override
  void initState() {
    super.initState();

    future = generalBloc.getCat();
    pageController = CarouselControllerImpl();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return FutureBuilder<CatResponse>(
      future: future,
      builder: (context, snapshot) {
        if(snapshot.hasError) {
          return Center(
            child: Column(
              children: [
                SizedBox(height: 50,),
                Text(snapshot.error.toString()),
              ],
            ),
          );
        } else if(snapshot.hasData) {
          return Container(
            width: size.width,
            height: size.height * .5,
            decoration: BoxDecoration(
              color: Color(0xffD04249),
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(30),
                bottomLeft: Radius.circular(30),
              ),
              boxShadow: [
                BoxShadow(
                  color: mainColor.withOpacity(.5),
                  offset: Offset(.7, .7),
                  spreadRadius: 2,
                  blurRadius: 6,
                ),
              ],
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      height: 70,
                    ),
                    IconButton(
                      icon: Icon(
                        Localizations.localeOf(context).languageCode == 'en' ? Icons.arrow_forward_ios_rounded : Icons.arrow_back_ios_rounded,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        pageController.previousPage();
                      },
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (_) => SearchPage()));
                      },
                      child: Container(
                        padding: EdgeInsets.all(10),
                        height: 60,
                        child: Icon(
                          Icons.search,
                          color: Colors.white,
                        ),
                        decoration: BoxDecoration(
                          color: Color(0xffEC2729),
                          borderRadius: BorderRadius.only(
                            bottomRight: Radius.circular(Localizations.localeOf(context).languageCode == 'ar' ? 30 : 0),
                            bottomLeft: Radius.circular(Localizations.localeOf(context).languageCode == 'en' ? 30 : 0),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Expanded(
                  child: Stack(
                    children: [
                      CarouselSlider(
                        carouselController: pageController,
                        options: CarouselOptions(
                          onPageChanged: (int page, _) {
                            setState(() {
                              currentPage = page;
                            });
                          },
                          height: double.infinity,
                          autoPlay: true,
                          viewportFraction: 1,
                        ),
                        items: snapshot.data.data.map((i) {
                            return Builder(
                              builder: (BuildContext context) {
                                return GestureDetector(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      PageTransition(
                                        type: PageTransitionType.bottomToTop,
                                        child: CategoryItemsDetailsPage(
                                          name: snapshot.data.data[currentPage].name,
                                          model: snapshot.data.data[currentPage],
                                        ),
                                      ),
                                    );
                                  },
                                  child: Column(
                                    children: [
                                      Expanded(
                                        child: Image.network(
                                          i.image,
                                          height: double.infinity,
                                          width: double.infinity,
                                          fit: BoxFit.fill,
                                        ),
                                      ),
                                      Text(
                                        i.name,
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 25,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                      SizedBox(height: 20,),
                                    ],
                                  ),
                                );
                              },
                            );
                          },
                        ).toList(),
                      ),
                      // Positioned(
                      //   bottom: 20,
                      //   left: 0,
                      //   right: 0,
                      //   child: Text(
                      //     snapshot.data.data[0].products[currentPage].name,
                      //     style: TextStyle(
                      //       color: Colors.white,
                      //       fontSize: 25,
                      //     ),
                      //     textAlign: TextAlign.center,
                      //   ),
                      // ),
                      Positioned(
                        bottom: 10,
                        left: 0,
                        right: 0,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: List.generate(
                            snapshot.data.data.length, (index) => AnimatedContainer(
                              duration: Duration(milliseconds: 200),
                              margin: EdgeInsets.symmetric(horizontal: 3),
                              height: 3,
                              width: 15,
                              color:
                              currentPage == index ? mainColor : Colors.white38,
                            ),
                          ).toList(),
                        ),
                      ),
                    ],
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      height: 70,
                    ),
                    IconButton(
                      icon: Icon(
                        Localizations.localeOf(context).languageCode == 'en' ?  Icons.arrow_back_ios_rounded : Icons.arrow_forward_ios_rounded,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        pageController.nextPage();
                      },
                    ),
                    GestureDetector(
                      onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(builder: (_) => CategoriesPage()));
                      },
                      child: Container(
                        padding: EdgeInsets.all(10),
                        height: 60,
                        child: Icon(
                          Icons.grid_view,
                          color: Colors.white,
                        ),
                        decoration: BoxDecoration(
                          color: Color(0xffEC2729),
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(Localizations.localeOf(context).languageCode == 'ar' ? 30 : 0),
                            bottomRight: Radius.circular(Localizations.localeOf(context).languageCode == 'en' ? 30 : 0),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          );
        }

        return Center(
          child: Column(
            children: [
              SizedBox(height: 50,),
              SpinKitCircle(
                color: mainColor,
              ),
            ],
          ),
        );
      }
    );
  }
}
