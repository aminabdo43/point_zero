
import 'package:flutter/material.dart';
import 'package:point_zero/Bles/Bloc/AuthBloc.dart';
import 'package:point_zero/Bles/Model/Responses/SliderResponse.dart';
import 'package:point_zero/pages/home/all_items_page.dart';
import 'package:point_zero/utils/app_utils.dart';
import 'package:point_zero/utils/consts.dart';

import 'store_item_model.dart';

class NewPart extends StatefulWidget {

  final String categoryName;
  final List<ProductsBean> data;

  const NewPart({Key key, this.categoryName, this.data}) : super(key: key);
  
  @override
  _NewPartState createState() => _NewPartState();
}

class _NewPartState extends State<NewPart> {

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return  Container(
      width: size.width,
      height: size.height * .4,
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 14),
            child: widget.data.isEmpty ? SizedBox.shrink() : Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  widget.categoryName,
                  style: TextStyle(color: mainColor, fontSize: 18),
                ),
                Row(
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (_) => AllItemsPage(
                              title: widget.categoryName,
                              list: widget.data,
                            ),
                          ),
                        );
                      },
                      child: Text(
                        AppUtils.translate(context, 'show_all'),
                        style: TextStyle(fontSize: 12),
                      ),
                    ),
                    SizedBox(
                      width: 3,
                    ),
                    RotatedBox(
                      quarterTurns: Localizations.localeOf(context).languageCode == 'en' ? 2 : 4,
                      child: Image.asset(
                        'assets/images/forward.png',
                        scale: 1.1,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(
                right: Localizations.localeOf(context).languageCode == 'ar' ? 18 : 0,
                left: Localizations.localeOf(context).languageCode == 'en' ? 18 : 0,
              ),
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                physics: bouncingScrollPhysics,
                itemBuilder: (context, index) {
                  if(widget.data.isNotEmpty) {
                    if(widget.data != null && widget.data.isNotEmpty) {
                      return generalBloc.slider.value == null || generalBloc.slider.value.data == null ? SizedBox.shrink() :
                      StoreItemModel(
                        title: widget.data[index].name,
                        categoryName: widget.data[index].keywords,
                        price: widget.data[index].price.toString(),
                        image: widget.data[index].mainImage,
                        productId: widget.data[index].id,
                        isNew: false,
                      );
                    } else {
                      return SizedBox.shrink();
                    }
                  } else {
                    return SizedBox.shrink();
                  }
                },
                itemCount: widget.data.length,
              ),
            ),
          ),
        ],
      ),
    );
  }

  void updateUI() {
    Future.delayed(Duration(milliseconds: 500), () {
      if(mounted) setState(() {});
    });
  }
}
