import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:point_zero/Bles/Bloc/AuthBloc.dart';
import 'package:point_zero/Bles/Model/Responses/SliderResponse.dart';
import 'package:point_zero/Bles/Model/Responses/products_discount_respones.dart';
import 'package:point_zero/utils/app_utils.dart';
import 'package:point_zero/utils/consts.dart';

import 'all_items_page.dart';
import 'store_item_model.dart';

class RecommendedPart extends StatefulWidget {
  @override
  _RecommendedPartState createState() => _RecommendedPartState();
}

class _RecommendedPartState extends State<RecommendedPart> {

  bool shuffle = false;

  Future future;
  List<Data> list = [];

  @override
  void initState() {
    super.initState();

    future = generalBloc.getProductsDiscount();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return  Container(
      width: size.width,
      height: size.height * .4,
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 14),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  AppUtils.translate(context, 'recommended'),
                  style: TextStyle(color: mainColor, fontSize: 18),
                ),
                Row(
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (_) => AllItemsPage(
                              title: AppUtils.translate(context, 'recommended'),
                              list: list,
                              isRecommended: true,
                            ),
                          ),
                        );
                      },
                      child: Text(
                        AppUtils.translate(context, 'show_all'),
                        style: TextStyle(fontSize: 12),
                      ),
                    ),
                    SizedBox(
                      width: 3,
                    ),
                    RotatedBox(
                      quarterTurns: Localizations.localeOf(context).languageCode == 'en' ? 2 : 4,
                      child: Image.asset(
                        'assets/images/forward.png',
                        scale: 1.1,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Expanded(
            child: FutureBuilder<ProductsDiscountRespones>(
              future: future,
              builder: (context, snapshot) {
                if(snapshot.hasError) {
                  return Center(
                    child: Text(snapshot.error.toString()),
                  );
                } else if(snapshot.hasData) {
                  if(!shuffle) {
                    shuffle = true;
                    list = snapshot.data.data;
                    list.shuffle();
                  }
                  return Padding(
                    padding: EdgeInsets.only(
                      right: Localizations.localeOf(context).languageCode == 'ar' ? 18 : 0,
                      left: Localizations.localeOf(context).languageCode == 'en' ? 18 : 0,
                    ),
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      physics: bouncingScrollPhysics,
                      itemBuilder: (context, index) {
                        if(list.isNotEmpty) {
                          if(list[0] != null && list.isNotEmpty) {
                            return index == list.length - 1
                                ? SizedBox(
                              width: 10,
                            ) : generalBloc.slider.value == null || generalBloc.slider.value.data == null ? SizedBox.shrink() :
                            StoreItemModel(
                              title: list[index].name,
                              categoryName: list[index].keywords,
                              price: list[index].price.toString(),
                              image: list[index].mainImage,
                              productId: list[index].id,
                              isNew: false,
                            );
                          } else {
                            return SizedBox.shrink();
                          }
                        } else {
                          return SizedBox.shrink();
                        }
                      },
                      itemCount: list.length,
                    ),
                  );
                }

                return Center(child: SpinKitCircle(color: mainColor, size: 30,),);
              },
            ),
          ),
        ],
      ),
    );
  }
}
