import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:point_zero/Bles/Bloc/AuthBloc.dart';
import 'package:point_zero/Bles/Model/Responses/ProductsResponse.dart';
import 'package:point_zero/pages/home/store_item_model.dart';
import 'package:point_zero/utils/app_utils.dart';
import 'package:point_zero/utils/consts.dart';
import 'package:point_zero/widgets/my_loader.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({Key key}) : super(key: key);

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {

  TextEditingController searchController = TextEditingController();
  List<String> searchKeys = [];

  Future future;

  @override
  void initState() {
    super.initState();

    generalBloc.searchFun('${searchController.text}');
  }

  @override
  Widget build(BuildContext context) {
    searchKeys = [
      AppUtils.translate(context, "dresses"),
      AppUtils.translate(context, "pajamas"),
      AppUtils.translate(context, "sports"),
      AppUtils.translate(context, "skirts"),
      AppUtils.translate(context, "veiled"),
    ];
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        body: Stack(children: [
      Container(
        width: size.width,
        height: size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: Image.asset(
              'assets/images/fav_bg.png',
              width: size.width,
              height: size.height,
            ).image,
            fit: BoxFit.cover,
          ),
        ),
          ),
          Container(
            width: size.width,
            height: size.height,
            color: mainColor.withOpacity(.35),
          ),
          Padding(
              padding: EdgeInsets.only(
              left: 14,
              right: 14,
              top: MediaQuery.of(context).padding.top + 10,
            ),
            child: Container(
              height: size.height,
              decoration: BoxDecoration(
                color: Colors.white,
                gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [
                    Color(0xffFAFAF9),
                    Color(0xffFDFCF9),
                    Color(0xffF9EFEA),
                  ],
                ),
                border: Border.all(color: mainColor),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                ),
              ),
              child: Column(
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    AppUtils.translate(context, 'search'),
                    style: TextStyle(color: mainColor, fontSize: 24),
                  ),
                  Divider(
                    color: mainColor,
                    height: 20,
                  ),
                  Padding(
                    padding: EdgeInsets.all(12.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          height: 50,
                          decoration: BoxDecoration(
                            color: Color(0xffE8D7D3).withOpacity(.5),
                            borderRadius: BorderRadius.circular(30),
                          ),
                          child: TextFormField(
                            onChanged: (input) {
                              setState(() {
                                future = generalBloc.searchFun('${searchController.text}');
                              });
                            },
                            onFieldSubmitted: (input) {
                              setState(() {
                                future = generalBloc.searchFun('${searchController.text}');
                              });
                            },
                            controller: searchController,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              contentPadding: EdgeInsets.only(top: 16, right: 16, left: 16),
                              suffixIcon: GestureDetector(
                                onTap: () {
                                  searchController.clear();
                                },
                                child: Icon(Icons.close, color: Colors.black, size: 18,),
                              ),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30),
                                  borderSide: BorderSide(
                                    color: Color(0xffE8D7D3).withOpacity(.5),
                                  ),
                                ),
                              ),
                          ),
                        ),
                        SizedBox(height: 20,),
                        Text('عمليات البحث الشائعة'),
                        SizedBox(height: 10,),
                        Container(
                          height: 40,
                          width: double.infinity,
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                              itemBuilder: (context, index) {
                                return GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      searchController.text = searchKeys[index];
                                      future = generalBloc.searchFun('${searchController.text}');
                                    });
                                  },
                                  child: Container(
                                    width: 100,
                                    margin: EdgeInsets.symmetric(horizontal: 4),
                                    decoration: BoxDecoration(
                                      color: Color(0xffF9DCDC),
                                      borderRadius: BorderRadius.circular(30),
                                    ),
                                    child: Center(child: Text(searchKeys[index], style: TextStyle(fontSize: 12, color: Colors.black),)),
                                  ),
                                );
                              },
                              itemCount: searchKeys.length,
                            ),
                          ),
                        SizedBox(height: 50,),
                        Container(
                          height: 250,
                          child: FutureBuilder<ProductsResponse>(
                            future: future,
                            builder: (context, snapshot) {
                              if(snapshot.hasData) {
                                return ListView.builder(
                                  physics: bouncingScrollPhysics,
                                  itemCount: snapshot.data.data.length,
                                  scrollDirection: Axis.horizontal,
                                  itemBuilder: (context, index) {
                                    return StoreItemModel(
                                      title: snapshot.data.data[index].name,
                                      categoryName: snapshot.data.data[index].keywords,
                                      price: snapshot.data.data[index].price.toString(),
                                      image: snapshot.data.data[index].mainImage,
                                      productId: snapshot.data.data[index].id,
                                      isNew: false,
                                    );
                                  },
                                );
                              } else {
                                return Loader(size: 40,);
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
      ],
    ),
      );
  }
}

