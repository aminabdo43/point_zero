import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:point_zero/Bles/Bloc/AuthBloc.dart';
import 'package:point_zero/Bles/Model/Responses/cart_item_model.dart';
import 'package:point_zero/pages/categories/single_item_details_page.dart';
import 'package:point_zero/utils/app_utils.dart';
import 'package:point_zero/utils/consts.dart';
import 'package:point_zero/utils/pointers.dart';

class StoreItemModel extends StatefulWidget {
  @required final String image;
  @required final String title;
  @required final String categoryName;
  @required final String price;
  @required final int productId;
  final String specialPrice;
  final bool isNew;

  const StoreItemModel({
    Key key,
    this.image,
    this.title,
    this.categoryName,
    this.price,
    this.specialPrice,
    this.productId,
    this.isNew = false,
  }) : super(key: key);

  @override
  _StoreItemModelState createState() => _StoreItemModelState();
}

class _StoreItemModelState extends State<StoreItemModel> {
  bool favourite = false;
  bool addToCart = false;

  CartItem ci;

  @override
  void initState() {
    super.initState();

    ci = CartItem(
      id: widget.productId.toString(),
      image: widget.image,
      price: widget.price,
      name: widget.title,
      orderNumber: widget.productId.toString(),
    );

    if(Pointers.cartItems.contains(ci)) {
      addToCart = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          PageTransition(
            child: SingleItemDetailsPage(
              productId: widget.productId,
            ),
            type: PageTransitionType.fade,
          ),
        );
      },
      child: Container(
        width: size.width * .4,
        margin: EdgeInsets.only(
          left: 18
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30),
            bottomRight: Radius.circular(30),
          ),
          border: Border.all(width: 1, color: mainColor),
        ),
        child: Column(
          children: [
            Expanded(
              child: Stack(
                children: [
                  ClipRRect(
                    child: Image.network(
                      widget.image,
                      fit: BoxFit.fill,
                      width: double.infinity,
                    ),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                      bottomRight: Radius.circular(30),
                    ),
                  ),
                  widget.specialPrice == null
                      ? SizedBox.shrink()
                      : Positioned(
                          left: 0,
                          child: Container(
                            height: 30,
                            width: 50,
                            child: Center(
                              child: Text(
                                '%10',
                                style:
                                    TextStyle(color: Colors.white, fontSize: 9),
                              ),
                            ),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topRight: Radius.circular(30),
                                topLeft: Radius.circular(50),
                                bottomRight: Radius.circular(30),
                              ),
                              color: mainColor,
                            ),
                          ),
                        ),
                  widget.isNew == null || widget.isNew == false
                      ? SizedBox.shrink()
                      : Positioned(
                    left: 0,
                    child: Container(
                      height: 30,
                      width: 50,
                      child: Center(
                        child: Text(
                          AppUtils.translate(context, 'new'),
                          style:
                          TextStyle(color: Colors.white, fontSize: 9),
                        ),
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(30),
                          topLeft: Radius.circular(50),
                          bottomRight: Radius.circular(30),
                        ),
                        color: mainColor,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        widget.title,
                        style: TextStyle(fontSize: 12),
                      ),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            addToCart = !addToCart;
                          });

                          if(addToCart) {
                            Pointers.addToCart(ci);
                            AppUtils.showToast(msg: 'added', bgColor: Colors.green);
                          } else {
                            AppUtils.showToast(msg: 'removed', bgColor: Colors.red);
                            Pointers.removeFromCart(ci);
                          }
                        },
                        child: Icon(
                          Icons.shopping_bag,
                          color: addToCart ? mainColor : Colors.grey,
                          size: 16,
                        ),
                      ),
                    ],
                  ),
                  Text(
                    widget.categoryName,
                    style: TextStyle(fontSize: 12, color: Colors.grey),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        '${widget.price}₪',
                        style: TextStyle(
                          fontSize: 12,
                          color: mainColor,
                          decorationStyle: TextDecorationStyle.wavy,
                          decorationThickness: 2,
                          decoration: widget.specialPrice == null ? TextDecoration.none : TextDecoration.lineThrough,
                        ),
                      ),
                      Text(
                        '${widget.specialPrice ?? ''}${widget.specialPrice == null ? '' : '₪'}',
                        style: TextStyle(fontSize: 12, color: mainColor),
                      ),
                      GestureDetector(
                        onTap: () {
                          if(AppUtils.userData == null) {
                            AppUtils.showToast(msg: 'login first', bgColor: Colors.red);
                            return;
                          }

                          setState(() {
                            favourite = !favourite;
                          });

                          if(favourite) {
                            generalBloc.add_wishList(widget.productId);
                          } else {
                            generalBloc.delete_wish_list(widget.productId);
                          }
                        },
                        child: Icon(
                          favourite ? Icons.favorite : Icons.favorite_border,
                          color: favourite ? Colors.red : Colors.grey,
                          size: 20,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
