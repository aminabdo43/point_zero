import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:point_zero/Bles/Bloc/AuthBloc.dart';
import 'package:point_zero/Bles/Model/Requests/MakeOrderRequest.dart';
import 'package:point_zero/pages/home/home_contents.dart';
import 'package:point_zero/pages/home/home_page.dart';
import 'package:point_zero/pages/payment/step_indicator_provider.dart';
import 'package:point_zero/providers/page_provider.dart';
import 'package:point_zero/utils/app_utils.dart';
import 'package:point_zero/utils/consts.dart';
import 'package:point_zero/utils/pointers.dart';
import 'package:point_zero/widgets/my_button.dart';
import 'package:provider/provider.dart';

class DonePage extends StatefulWidget {
  @override
  _DonePageState createState() => _DonePageState();
}

class _DonePageState extends State<DonePage> {

  @override
  void initState() {
    super.initState();

    generalBloc.make_Order(MakeOrderRequest(
      address: Pointers.address == null || Pointers.address.isEmpty ? AppUtils.userData.userBean.address1 : Pointers.address,
      cart: Pointers.cartItems,
      country: AppUtils.userData.userBean.country,
      coupon: AppUtils.coupon,
      mobile: AppUtils.userData.userBean.mobile,
      paymentMethod: 'cash',
      user: AppUtils.userData.userBean.id,
    )).then((value) => log(value.toString()));


    Pointers.clearItems();
    Pointers.cartItems.clear();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        width: size.width,
        height: size.height,
        decoration: BoxDecoration(
          gradient: RadialGradient(
            colors: [
              Colors.white,
              Color(0xffE8E6E4),
            ],
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 40,
            ),
            Image.asset('assets/images/done_order.png', scale: 1.1,),
            SizedBox(
              height: 10,
            ),
            Text(
             AppUtils.translate(context, 'done_msg'),
              style: TextStyle(
                color: mainColor,
                fontSize: 25,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 8,),
            // Text(
            //   '${AppUtils.translate(context, 'order_number')} : 1525-4545 \n${AppUtils.translate(context, 'order_submitted')}',
            //   textAlign: TextAlign.center,
            //   style: TextStyle(
            //     fontSize: 16,
            //   ),
            // ),
            SizedBox(
              height: 80,
            ),
            myButton(
              AppUtils.translate(context, 'back_to_shopping'),
              context,
              width: size.width / 2,
              onTap: () {
                Provider.of<StepIndicatorProvider>(context, listen: false).setStep(1);
                Provider.of<PageProvider>(context, listen: false).setPage(0, HomeContents());
                Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (_) => HomePage()), (route) => false);
              },
            ),
          ],
        ),
      ),
    );
  }
}
