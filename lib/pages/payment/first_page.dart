import 'package:flutter/material.dart';
import 'package:point_zero/utils/app_utils.dart';
import 'package:point_zero/utils/consts.dart';
import 'package:point_zero/widgets/my_text_form_field.dart';

class FirstPage extends StatefulWidget {
  @override
  _FirstPageState createState() => _FirstPageState();
}

class _FirstPageState extends State<FirstPage> {

  bool sameAddress = false;

  List<String> countries = [
    'الضفة الغربية',
    "القدس",
    "اراضي 48",
  ];

  String selectedCountry;
  TextEditingController street1Controller = TextEditingController(text: AppUtils.userData.userBean.address1);
  TextEditingController street2Controller = TextEditingController();
  TextEditingController hyController = TextEditingController();
  TextEditingController cityController = TextEditingController();
  TextEditingController countryController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(12.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 20,
          ),
          // ListTile(
          //   onTap: () {
          //     setState(() {
          //       sameAddress = !sameAddress;
          //     });
          //   },
          //   leading: Container(
          //     width: 30,
          //     height: 30,
          //     decoration: BoxDecoration(
          //       color: mainColor,
          //       shape: BoxShape.circle,
          //     ),
          //     child: sameAddress
          //         ? Icon(
          //             Icons.check,
          //             color: Colors.white,
          //             size: 18,
          //           )
          //         : SizedBox.shrink(),
          //   ),
          //   title: Text(
          //     AppUtils.translate(context, 'payment_address_msg'),
          //   ),
          // ),
          // SizedBox(
          //   height: 40,
          // ),
          Text(AppUtils.translate(context, 'address'),),
          MyTextFormField(
            controller: street1Controller,
            onChanged: (String input) {
              AppUtils.userData.userBean.address1 = input;
            },
          ),
          // SizedBox(
          //   height: 40,
          // ),
          // Text(AppUtils.translate(context, 'street_2')),
          // MyTextFormField(
          //   controller: street2Controller,
          // ),
          // SizedBox(
          //   height: 40,
          // ),
          // Text(AppUtils.translate(context, 'neighborhood')),
          // MyTextFormField(
          //   controller: hyController,
          // ),
          // SizedBox(
          //   height: 40,
          // ),
          // Text(AppUtils.translate(context, 'city')),
          // MyTextFormField(
          //   controller: cityController,
          // ),
          // SizedBox(
          //   height: 40,
          // ),
          // Text(AppUtils.translate(context, 'country')),
          // Container(
          //   width: double.infinity,
          //   padding: EdgeInsets.only(top: 14),
          //   child: DropdownButton<String>(
          //     isExpanded: true,
          //     elevation: 4,
          //     value: selectedCountry,
          //     underline: Container(width: double.infinity, height: 1.5, color: Colors.grey,),
          //     items: countries.map((String value) {
          //       return DropdownMenuItem<String>(
          //         value: value,
          //         child: Container(
          //           child: Text(value),
          //           alignment:
          //           Localizations.localeOf(context)
          //               .languageCode ==
          //               'ar'
          //               ? Alignment.centerRight
          //               : Alignment.centerLeft,
          //         ),
          //       );
          //     }).toList(),
          //     onChanged: (newValue) {
          //       setState(() {
          //         selectedCountry = newValue;
          //         countryController.text = selectedCountry;
          //       });
          //     },
          //   ),
          // ),
          SizedBox(
            height: 40,
          ),
        ],
      ),
    );
  }
}
