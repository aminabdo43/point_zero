import 'package:flutter/material.dart';
import 'package:point_zero/pages/cart/cart_page.dart';
import 'package:point_zero/pages/payment/done_page.dart';
import 'package:point_zero/pages/payment/first_page.dart';
import 'package:point_zero/pages/payment/step_indicator_provider.dart';
import 'package:point_zero/utils/app_utils.dart';
import 'package:point_zero/utils/consts.dart';
import 'package:provider/provider.dart';
import 'package:steps_indicator/steps_indicator.dart';

import 'second_page.dart';
import 'third_page.dart';

class MainPaymentPage extends StatefulWidget {
  @override
  _MainPaymentPageState createState() => _MainPaymentPageState();
}

class _MainPaymentPageState extends State<MainPaymentPage> {
  ScrollController scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    var prov = Provider.of<StepIndicatorProvider>(context, listen: false);
    return WillPopScope(
      onWillPop: () async {
        if (prov.selectedStep > 1) {
          prov.setStep(prov.selectedStep - 1);
          return false;
        } else {
          return true;
        }
      },
      child: Scaffold(
        body: Builder(
          builder: (context) => Consumer<StepIndicatorProvider>(
            builder: (context, provider, child) => Stack(
              children: [
                Container(
                  width: size.width,
                  height: size.height,
                  decoration: BoxDecoration(
                    gradient: RadialGradient(
                      colors: [
                        Colors.white,
                        Color(0xffE8E6E4),
                      ],
                    ),
                  ),
                ),
                SingleChildScrollView(
                  controller: scrollController,
                  physics: bouncingScrollPhysics,
                  child: Column(
                    children: [
                      SizedBox(
                        height: MediaQuery.of(context).padding.top,
                      ),
                      Stack(
                        children: [
                          appBar(
                            context,
                            withSearch: false,
                            withGrid: false,
                            center: true,
                            title: AppUtils.translate(context, 'payment'),
                          ),
                          Positioned(
                            child: GestureDetector(
                              child: Icon(Icons.arrow_forward, color: mainColor,),
                              onTap: () {
                                Navigator.pop(context);
                              },
                            ),
                            left: 10,
                            top: 35,
                          )
                        ],
                      ),
                      SizedBox(
                        height: size.width * .05,
                      ),
                      StepsIndicator(
                        selectedStep: provider.selectedStep,
                        nbSteps: 3,
                        selectedStepColorOut: Colors.white,
                        selectedStepColorIn: Colors.white,
                        doneStepColor: Colors.white,
                        doneLineColor: mainColor,
                        undoneLineColor: Colors.grey,
                        isHorizontal: true,
                        lineLength: (MediaQuery.of(context).size.width / 3 + 10),
                        doneStepSize: 10,
                        unselectedStepSize: 10,
                        selectedStepSize: 14,
                        selectedStepBorderSize: 1,
                        unselectedStepColorOut: Colors.white,
                        unselectedStepColorIn: Colors.white,
                        doneStepWidget: Container(
                          width: 30,
                          height: 30,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.white,
                            border: Border.all(
                              color: Colors.white,
                              width: 1,
                            ),
                          ),
                          child: Center(
                            child: Container(
                              width: (15),
                              height: (15),
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: mainColor,
                                border: Border.all(
                                  color: mainColor,
                                  width: 1,
                                ),
                              ),
                            ),
                          ),
                        ),
                        unselectedStepWidget: Container(
                          width: (30),
                          height: (30),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                            border: Border.all(
                              color: Colors.white,
                              width: 1,
                            ),
                          ),
                        ),
                        selectedStepWidget: Container(
                          width: (30),
                          height: (30),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                            border: Border.all(
                              color: Colors.white,
                              width: 1,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              AppUtils.translate(context, 'address'),
                              style: TextStyle(
                                color: provider.selectedStep == 1
                                    ? mainColor
                                    : Colors.black,
                              ),
                            ),
                            Text(
                              AppUtils.translate(context, 'payment'),
                              style: TextStyle(
                                color: provider.selectedStep == 2 ? mainColor : Colors.black,
                              ),
                            ),
                            Text(
                              AppUtils.translate(context, 'submit'),
                              style: TextStyle(color: provider.selectedStep == 3 ? mainColor : Colors.black),
                            ),
                          ],
                        ),
                      ),
                      provider.selectedStep == 1 ? FirstPage() : provider.selectedStep == 2 ? SecondPage() : ThirdPage(),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: List.generate(
                          provider.selectedStep == 1 ? 1 : 2, (index) => GestureDetector(
                            onTap: () {
                              if (index == 0) {
                                if (provider.selectedStep < 3) {
                                  scrollController.animateTo(
                                    scrollController.position.minScrollExtent,
                                    duration: Duration(milliseconds: 500),
                                    curve: Curves.easeOut,
                                  );
                                  provider.setStep(provider.selectedStep + 1);
                                } else {
                                  Navigator.of(context).push(
                                    MaterialPageRoute(
                                      builder: (_) => DonePage(),
                                    ),
                                  ).then((value) {
                                    provider.setStep(0);
                                  });
                                }
                              } else {
                                if (provider.selectedStep > 1) {
                                  provider.setStep(provider.selectedStep - 1);
                                  scrollController.animateTo(
                                    scrollController.position.minScrollExtent,
                                    duration: Duration(milliseconds: 500),
                                    curve: Curves.easeOut,
                                  );
                                }
                              }
                            },
                            child: AnimatedContainer(
                              width: 140,
                              height: 50,
                              duration: Duration(milliseconds: 300),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(30),
                                border: Border.all(color: mainColor, width: 1),
                                color: index == 0 ? mainColor : Colors.transparent,
                              ),
                              child: Padding(
                                padding: EdgeInsets.all(8.0),
                                child: Center(
                                  child: Text(
                                    index == 0 ? provider.selectedStep == 3 ? AppUtils.translate(context, 'pay') : AppUtils.translate(context, 'next') : AppUtils.translate(context, 'previous'),
                                    style: TextStyle(
                                      color: index == 0 ? Colors.white : Colors.black,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
