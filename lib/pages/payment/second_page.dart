import 'package:flutter/material.dart';
import 'package:point_zero/utils/app_utils.dart';
import 'package:point_zero/utils/consts.dart';
import 'package:point_zero/widgets/my_text_form_field.dart';

class SecondPage extends StatefulWidget {
  @override
  _SecondPageState createState() => _SecondPageState();
}

class _SecondPageState extends State<SecondPage> {
  int paymentType = 1;

  FocusNode nameNode = FocusNode();
  FocusNode cardNumberNode = FocusNode();
  FocusNode endDateNode = FocusNode();
  FocusNode cvvNode = FocusNode();

  bool saveCreditCardData = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(12.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          AnimatedContainer(
            width: 120,
            height: 50,
            padding: EdgeInsets.all(8),
            duration: Duration(milliseconds: 300),
            decoration: BoxDecoration(
              color: mainColor,
              borderRadius: BorderRadius.circular(30),
            ),
            child: Icon(
              Icons.money,
              color: Colors.white,
            ),
          ),
          SizedBox(height: 10,),
          Text(AppUtils.translate(context, 'cash'),
            textAlign: TextAlign.center,
          ),
          // Row(
          //   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          //   children: List.generate(
          //     2,
          //     (index) => GestureDetector(
          //       onTap: () {
          //         paymentType = index;
          //         setState(() {});
          //       },
          //       child: Column(
          //         children: [
          //           AnimatedContainer(
          //             width: 120,
          //             height: 50,
          //             padding: EdgeInsets.all(8),
          //             duration: Duration(milliseconds: 300),
          //             decoration: BoxDecoration(
          //               color: index == paymentType ? mainColor : Colors.white,
          //               borderRadius: BorderRadius.circular(30),
          //             ),
          //             child: Icon(
          //               index == 0 ? Icons.money : Icons.credit_card,
          //               color:
          //                   index == paymentType ? Colors.white : Colors.black,
          //             ),
          //           ),
          //           Text(
          //             index == 0
          //                 ? AppUtils.translate(context, 'cash')
          //                 : AppUtils.translate(context, 'credit_card'),
          //             textAlign: TextAlign.center,
          //           ),
          //         ],
          //       ),
          //     ),
          //   ),
          // ),
          // SizedBox(
          //   height: 40,
          // ),
          // Text(AppUtils.translate(context, 'name_on_card')),
          // MyTextFormField(
          //   focusNode: nameNode,
          // ),
          // SizedBox(
          //   height: 40,
          // ),
          // Text(
          //   AppUtils.translate(context, 'card_number'),
          // ),
          // MyTextFormField(
          //   focusNode: cardNumberNode,
          // ),
          // SizedBox(
          //   height: 40,
          // ),
          // Row(
          //   children: [
          //     Expanded(
          //       child: Column(
          //         crossAxisAlignment: CrossAxisAlignment.start,
          //         children: [
          //           Text(AppUtils.translate(context, 'expire_date'),),
          //           MyTextFormField(
          //             focusNode: endDateNode,
          //           ),
          //         ],
          //       ),
          //     ),
          //     SizedBox(
          //       width: 12,
          //     ),
          //     Expanded(
          //       child: Column(
          //         crossAxisAlignment: CrossAxisAlignment.start,
          //         children: [
          //           Text(AppUtils.translate(context, 'cvv'),),
          //           MyTextFormField(
          //             focusNode: cvvNode,
          //           ),
          //         ],
          //       ),
          //     ),
          //   ],
          // ),
          // SizedBox(
          //   height: 40,
          // ),
          // ListTile(
          //   onTap: () {
          //     setState(() {
          //       saveCreditCardData = !saveCreditCardData;
          //     });
          //   },
          //   leading: Container(
          //     width: 30,
          //     height: 30,
          //     decoration: BoxDecoration(
          //       color: mainColor,
          //       shape: BoxShape.circle,
          //     ),
          //     child: saveCreditCardData
          //         ? Icon(
          //             Icons.check,
          //             color: Colors.white,
          //             size: 18,
          //           )
          //         : SizedBox.shrink(),
          //   ),
          //   title: Text(AppUtils.translate(context, 'save_credit_data'),),
          // ),
          SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }
}
