
import 'package:flutter/foundation.dart';

class StepIndicatorProvider with ChangeNotifier{
  int _selectedStep = 1;

  int get selectedStep => _selectedStep;

  void setStep(int step) {
    _selectedStep = step;
    notifyListeners();
  }
}