import 'package:flutter/material.dart';
import 'package:point_zero/utils/app_utils.dart';
import 'package:point_zero/utils/consts.dart';
import 'package:point_zero/utils/pointers.dart';

class ThirdPage extends StatefulWidget {
  @override
  _ThirdPageState createState() => _ThirdPageState();
}

class _ThirdPageState extends State<ThirdPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Padding(
        padding: EdgeInsets.all(12.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    AppUtils.translate(context, 'total'),
                    style: TextStyle(
                      color: mainColor,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    '₪${Pointers.getTotalPrice() - Pointers.couponDiscountValue}',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 20,),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 180,
              child: ListView.separated(
                physics: bouncingScrollPhysics,
                itemBuilder: (context, index) {
                  return Column(
                    children: [
                      ClipRRect(
                        child: Image.network(
                          Pointers.cartItems[index].image,
                          height: 120,
                          fit: BoxFit.fill,
                        ),
                        borderRadius: BorderRadius.circular(20),
                      ),
                      Text(Pointers.cartItems[index].name ?? ''),
                      Text('₪${Pointers.cartItems[index].price ?? ''}', style: TextStyle(color: mainColor,),),
                    ],
                  );
                },
                itemCount: Pointers.cartItems.length,
                scrollDirection: Axis.horizontal,
                separatorBuilder: (BuildContext context, int index) {
                  return SizedBox(
                    width: 12,
                  );
                },
              ),
            ),
            ListTile(
              // trailing: Container(
              //   width: 25,
              //   height: 25,
              //   decoration: BoxDecoration(
              //     color: mainColor,
              //     shape: BoxShape.circle,
              //   ),
              //   child: Icon(
              //     Icons.check,
              //     color: Colors.white,
              //     size: 16,
              //   ),
              // ),
              title: Text(AppUtils.translate(context, 'address'),),
              subtitle: Text(AppUtils.userData.userBean.address1 ?? ''),
            ),
            Divider(
              height: 30,
              thickness: 1,
            ),
            // Image.asset('assets/images/Payment.png', width: MediaQuery.of(context).size.width, fit: BoxFit.cover,),
            SizedBox(height: 20,),
          ],
        ),
      ),
    );
  }
}
