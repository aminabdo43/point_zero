import 'package:flutter/material.dart';
import 'package:point_zero/pages/auth/login_page.dart';
import 'package:point_zero/pages/home/home_page.dart';
import 'package:point_zero/pages/welcome/welcome_page.dart';
import 'package:point_zero/utils/app_utils.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();

    Future.delayed(
      Duration(seconds: 3),
      () {
        Navigator.pushAndRemoveUntil(context,
            MaterialPageRoute(builder: (_) => AppUtils.userData == null ? LoginPage() : HomePage()), (route) => false);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        width: size.width,
        height: size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: Image.asset('assets/images/splash_bg.png').image,
            fit: BoxFit.fill,
          ),
        ),
        child: Container(
          width: size.width,
          height: size.height,
          child: Stack(
            children: [
              Container(
                color: Colors.black54,
                width: size.width,
                height: size.height,
              ),
              Center(
                child: Image.asset('assets/images/logo.png'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
