import 'package:flutter/material.dart';
import 'package:point_zero/pages/auth/login_page.dart';
import 'package:point_zero/utils/app_utils.dart';
import 'package:point_zero/utils/consts.dart';
import 'package:point_zero/widgets/my_button.dart';

class WelcomePage extends StatefulWidget {
  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        width: size.width,
        height: size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image:
                Image.asset('assets/images/enable_notifications_bg.png').image,
            fit: BoxFit.fill,
          ),
        ),
        child: Container(
          width: size.width,
          height: size.height,
          child: Stack(
            children: [
              Container(
                color: Colors.black54,
                width: size.width,
                height: size.height,
              ),
              Container(
                width: size.width,
                height: size.height,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 25),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: MediaQuery.of(context).padding.top + 15,
                      ),
                      Align(
                        alignment: Localizations.localeOf(context).languageCode == 'ar' ? Alignment.topLeft : Alignment.topRight,
                        child: GestureDetector(
                          onTap: () {
                            Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (_) => LoginPage()), (route) => false);
                          },
                          child: Icon(
                            Icons.close, color: mainColor,
                            size: 28,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: size.width / 2.8 - 50,
                      ),
                      Text(
                        AppUtils.translate(context, 'enable_notifications_msg'),
                        style: TextStyle(
                          fontSize: 22,
                          color: Colors.white,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        height: 50,
                      ),
                      ListTile(
                        leading: Image.asset('assets/images/orders.png'),
                        title: Text(
                          AppUtils.translate(context, 'order_status'),
                          style: TextStyle(
                            fontSize: 16,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      ListTile(
                        leading: Image.asset(
                          'assets/images/storage.png',
                          scale: 1.4,
                        ),
                        title: Text(
                          AppUtils.translate(context, 'storage_notifications'),
                          style: TextStyle(
                            fontSize: 16,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      ListTile(
                        leading: Image.asset('assets/images/activity.png'),
                        title: Text(
                          AppUtils.translate(context, 'personal_activity'),
                          style: TextStyle(
                            fontSize: 16,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      Expanded(
                        child: Align(
                          child: myButton(
                            AppUtils.translate(context, 'enable'),
                            context,
                            onTap: () {
                              Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(builder: (_) => LoginPage()),
                                (route) => false,
                              );
                            },
                          ),
                          alignment: Alignment.bottomCenter,
                        ),
                      ),
                      SizedBox(
                        height: size.width / 5,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
