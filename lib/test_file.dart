import 'dart:math';

import 'package:flutter/material.dart';

class MyTestFile extends StatefulWidget {
  @override
  _MyTestFileState createState() => _MyTestFileState();
}

class _MyTestFileState extends State<MyTestFile> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: LayoutBuilder(
        builder: (context, constraint) => SingleChildScrollView(
          child: Column(
            children: List.generate(
              5,
              (index) => Transform.translate(
                offset: Offset(0, (index * -65).toDouble()),
                child: Container(
                  width: size.width,
                  height: 250,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.fill,
                      image: Image.asset('assets/images/2girl.png').image,
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey,
                        blurRadius: 9,
                        offset: Offset(.7, .7),
                        spreadRadius: 2,
                      ),
                    ],
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(index == 0 ? 0 : 60),
                      topRight: Radius.circular(index == 0 ? 0 : 60),
                    ),
                  ),
                ),
              ),
            ).toList(),
          ),
        ),
      ),
    );
  }
}
