import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:point_zero/Bles/api_provider/BaseApiProvider.dart';

class BaseRepository{

  BaseApiProvider _apiProvider = BaseApiProvider();

  Future<Response> get(String urlExtention) async {
    log("url -------------------------->>>> $urlExtention");
    Response response = await _apiProvider.generalGet(urlExtention);
    return response;
  }

  Future<Response> post(String urlExtention, request, {bool isForm = false}) async {
    debugPrint("BaseRepository post request ---->>>>>>   ");
    debugPrint(json.encode(request.toString()));

    Response response = await _apiProvider.generalPost(urlExtention, request, isForm: isForm);

    debugPrint("BaseRepository post response ---->>>>>>   ");
    debugPrint(response.toString());
    return response;
  }
}