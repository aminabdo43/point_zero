
import 'package:flutter/material.dart';

final String appName = 'Point Zero';

final Color mainColor = Color(0xffFA4248);
final Color secondColor = Color(0xff323C47);
final Color thirdColor = Color(0xff323C47);
final Color greyColor = Color(0xffCBC6C2);
final Color scaffoldBackgroundColor = Color(0xffF7F7F7);

ScrollPhysics bouncingScrollPhysics = BouncingScrollPhysics();