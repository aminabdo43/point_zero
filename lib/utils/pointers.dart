
import 'dart:convert';

import 'package:point_zero/Bles/Model/Responses/cart_item_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Pointers {

  static List<CartItem> cartItems = [];
  static SharedPreferences sharedPreferences;
  static double totalPrice = 0.0;
  static double couponDiscountValue = 0.0;
  static String address = '';

  static void addToCart(CartItem cartItem) async {
    if(cartItems.contains(cartItem)) return;

    cartItems.add(cartItem);
    if(sharedPreferences == null) {
      sharedPreferences = await SharedPreferences.getInstance();
    }

    List<String> items = [];
    items.add(json.encode(cartItem.toJsonForSaving()));
    sharedPreferences.setStringList('cartItems', items);
  }

  static void updateItemQuantity(CartItem cartItem, int quantity) async {
    cartItems.firstWhere((element) => element.id.toString() == cartItem.id.toString())..quantity = quantity;
    if(sharedPreferences == null) {
      sharedPreferences = await SharedPreferences.getInstance();
    }

    List<String> items = [];
    items.add(json.encode(cartItem.toJsonForSaving()));
    sharedPreferences.setStringList('cartItems', items);
  }

  static Future<void> removeFromCart(CartItem cartItem) async {
    cartItems.remove(cartItem);

    if(sharedPreferences == null) {
      sharedPreferences = await SharedPreferences.getInstance();
    }

    List<String> items = [];
    items.add(json.encode(cartItem.toJsonForSaving()));
    sharedPreferences.setStringList('cartItems', items);
  }

  static Future<List<CartItem>> getStoredItems() async {
    if(sharedPreferences == null) {
      sharedPreferences = await SharedPreferences.getInstance();
    }

    List<String> items = sharedPreferences.getStringList('cartItems') ?? [];
    if(items.isNotEmpty) {
      items.forEach((element) {
        cartItems.add(CartItem.fromJsonSaving(json.decode(element)));
      });
    }

    return cartItems;
  }

  static void clearItems() async {
    if(sharedPreferences == null) {
      sharedPreferences = await SharedPreferences.getInstance();
    }

    sharedPreferences.setStringList('cartItems', []);
  }

  static double getTotalPrice() {
    totalPrice = 0.0;
    if(cartItems.isNotEmpty) {
      cartItems.forEach((element) {
        totalPrice += double.parse(double.parse(element.price).toStringAsFixed(4)) * element.quantity;
      });
    }

    return totalPrice;
  }
}