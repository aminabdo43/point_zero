
import 'package:flutter/material.dart';
import 'package:point_zero/pages/account/account_page.dart';
import 'package:point_zero/pages/cart/cart_page.dart';
import 'package:point_zero/pages/favourite/favourite_page.dart';
import 'package:point_zero/pages/home/home_contents.dart';
import 'package:point_zero/providers/page_provider.dart';
import 'package:point_zero/utils/consts.dart';
import 'package:provider/provider.dart';

class MyBottomNavigatonBar extends StatefulWidget {
  @override
  _MyBottomNavigatonBarState createState() => _MyBottomNavigatonBarState();
}

class _MyBottomNavigatonBarState extends State<MyBottomNavigatonBar> {

  @override
  Widget build(BuildContext context) {
    return Consumer<PageProvider>(
      builder: (context, provider, child) => ClipRRect(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30),
          topRight: Radius.circular(30),
        ),
        child: Container(
          height: 70,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30),
              topRight: Radius.circular(30),
            ),
            color: Colors.white,
            border: Border.all(color: mainColor, width: 1.5),
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30),
              topRight: Radius.circular(30),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                IconButton(icon: Icon(Icons.home_filled, color: provider.pageIndex == 0 ? mainColor : Colors.grey,), onPressed: () {
                  provider.setPage(0, HomeContents());
                }),
                IconButton(icon: Icon(Icons.shopping_cart, color: provider.pageIndex == 1 ? mainColor : Colors.grey,), onPressed: () {
                  provider.setPage(1, CartPage());
                }),
                IconButton(icon: Icon(Icons.favorite_border, color: provider.pageIndex == 2 ? mainColor : Colors.grey,), onPressed: () {
                  provider.setPage(2, FavouritePage());
                }),
                IconButton(icon: Icon(Icons.person_outline_outlined, color: provider.pageIndex == 3 ? mainColor : Colors.grey,), onPressed: () {
                  provider.setPage(3, AccountPage());
                }),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void goToPage(Widget page) {
    Navigator.of(context).push(MaterialPageRoute(builder: (_) => page));
  }
}