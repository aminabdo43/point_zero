import 'package:flutter/material.dart';
import 'package:point_zero/utils/consts.dart';

class ClickableText extends StatelessWidget {
  final String text;
  final Function onTap;
  final TextStyle textStyle;
  final Color color;

  ClickableText({Key key, @required this.text, this.onTap, this.textStyle, this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Text(
        text,
        style: textStyle ?? TextStyle(
          color: color ?? mainColor,
        ),
      ),
    );
  }
}
