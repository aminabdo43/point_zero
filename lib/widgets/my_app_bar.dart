import 'package:flutter/material.dart';
import 'package:point_zero/utils/consts.dart';

class MyAppBar extends StatelessWidget {
  final String title;
  final Widget leadingIcon;
  final Function onLeadingIconPressed;

  const MyAppBar({Key key, @required this.title, this.leadingIcon, this.onLeadingIconPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Stack(
          children: [
            Container(
              width: double.infinity,
              padding: EdgeInsets.all(12),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                ),
              ),
              child: Center(
                child: Text(
                  title,
                  style: TextStyle(color: mainColor, fontSize: 22),
                ),
              ),
            ),
            Positioned(
              top: 8,
              child: leadingIcon ?? IconButton(
                icon: Icon(Icons.close, color: mainColor,),
                onPressed: onLeadingIconPressed ?? () {
                  Navigator.pop(context);
                },
              ),
            ),
          ],
        ),
        Divider(
          thickness: 1,
          height: 1,
          color: mainColor,
        ),
      ],
    );
  }
}
