import 'package:flutter/material.dart';
import 'package:point_zero/utils/consts.dart';

Widget myButton(
  String title,
  BuildContext context, {
  Function onTap,
  Color btnColor,
  BoxDecoration decoration,
  double width,
  TextStyle textStyle,
  double height,
  EdgeInsets margin,
}) {
  return Container(
    width: width ??MediaQuery.of(context).size.width / 3,
    margin: margin,
    height: (height ?? 55),
    decoration: decoration ??
        BoxDecoration(
          color: btnColor ?? mainColor,
          borderRadius: BorderRadius.circular(50),
        ),
    child: ClipRRect(
      borderRadius: BorderRadius.circular(50),
      child: FlatButton(
        padding: EdgeInsets.zero,
        onPressed: onTap,
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: (12),
          ),
          child: Text(
            title,
            textAlign: TextAlign.center,
            style: textStyle ??
                TextStyle(
                  color: Colors.white,
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
          ),
        ),
      ),
    ),
  );
}
