// import 'package:flutter/material.dart';
// import 'package:qimma_client/utils/consts.dart';
//
// class MyCircleButton extends StatelessWidget {
//   final Function onTap;
//   final Widget child;
//   final double elevation;
//   final Color color;
//   final bool mini;
//
//   const MyCircleButton(
//       {Key key, this.onTap, this.elevation, this.color, this.mini, this.child})
//       : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return FloatingActionButton(
//       heroTag: '${DateTime.now()} : ${DateTime.may}',
//       onPressed: onTap ?? () {},
//       elevation: elevation ?? 0,
//       focusElevation: elevation ?? 0,
//       highlightElevation: elevation ?? 0,
//       backgroundColor: color ?? secondColor,
//       mini: mini ?? true,
//       child: child ??
//           Icon(
//             Icons.arrow_back,
//             color: Colors.black,
//             size: 18,
//           ),
//     );
//   }
// }
