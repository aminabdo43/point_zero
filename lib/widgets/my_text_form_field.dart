import 'package:flutter/material.dart';
import 'package:point_zero/utils/consts.dart';

class MyTextFormField extends StatefulWidget {
  final String hintText;
  final bool obscureText;
  final String label;
  final Function(String input) validator, onChanged, onSubmit;
  final TextEditingController controller;
  final TextInputType keyboardType;
  final int maxLines;
  final Color borderColor;
  final TextStyle lableStyle;
  final TextStyle hintStyle;
  final double borderWidth;
  final BoxDecoration decoration;
  final FocusNode focusNode;
  final Widget suffixIcon;
  final Widget prefixIcon;
  final Color bgColor;

  const MyTextFormField({
    Key key,
    this.hintText = '',
    this.maxLines,
    this.obscureText = false,
    this.validator,
    this.controller,
    this.keyboardType,
    this.onChanged,
    this.label = '',
    this.onSubmit,
    this.borderColor,
    this.borderWidth,
    this.lableStyle,
    this.decoration,
    this.hintStyle,
    this.focusNode,
    this.suffixIcon,
    this.prefixIcon,
    this.bgColor,
  }) : super(key: key);

  @override
  _MyTextFormFieldState createState() => _MyTextFormFieldState();
}

class _MyTextFormFieldState extends State<MyTextFormField> {
  bool hasFocus = false;

  @override
  void initState() {
    super.initState();

    if (widget.focusNode != null) {
      if (widget.focusNode.hasFocus) {
        hasFocus = true;
      } else {
        hasFocus = false;
      }

      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TextFormField(
          textAlign: TextAlign.justify,
          maxLines: widget.maxLines ?? 1,
          keyboardType: widget.keyboardType,
          onFieldSubmitted: widget.onSubmit,
          focusNode: widget.focusNode,
          decoration: InputDecoration(
            hintText: widget.hintText,
            hintStyle: widget.hintStyle ?? TextStyle(fontSize: 14),
            alignLabelWithHint: true,
            suffixIcon: widget.suffixIcon,
            prefixIcon: widget.prefixIcon,
          ),
          obscureText: widget.obscureText,
          controller: widget.controller,
          validator: widget.validator,
          onChanged: widget.onChanged,
        ),
        Container(
          width: double.infinity,
          height: 1,
          color: hasFocus ? mainColor : greyColor,
        ),
      ].where((element) => element != null).toList(),
    );
  }
}
